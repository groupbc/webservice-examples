# Python Examples

| Topic                        | Script                                               | Guide                                                |
|------------------------------|------------------------------------------------------|------------------------------------------------------|
| Authentication               | [authenticate.py](authenticate.py)                   | [authenticate.md](../guides/authenticate.md)         |
| JWT Authentication           | [authenticate_jwt.py](authenticate_jwt.py)           | [authenticate_jwt.md](../guides/authenticate_jwt.md) |
| Creating a Project Workspace | [create_project.py](create_project.py)               | [create_project.md](../guides/create_project.md)     |
| Creating a Document          | [create_document.py](create_document.py)             | [create_document.md](../guides/create_document.md)   |
| Getting and Setting Metadata | [set_metadata.py](set_metadata.py)                   | [set_metadata.md](../guides/set_metadata.md)         |
| Performing a Simple Search   | [simple_search.py](simple_search.py)                 | [simple_search.md](../guides/simple_search.md)       |
| Streaming Files              | [create_a_big_document.py](create_a_big_document.py) | [streaming_files.md](../guides/streaming_files.md)   |

## <a name="#required-libraries"></a> Required Libraries

The examples are compatible with Python 3.9.
We use the `requests` and `requests_oauthlib` python libraries to handle basic
OAuth functionality. `python-jose` is used for creating JWTs. All libraries 
used in these examples can be installed by running the following:

```
pip3.9 install -r requirements.txt
```

To install these requirements in their own isolated environment you may create
a [virtual environment](https://docs.python.org/3/library/venv.html#venv-def)
by running the following:

```
python3.9 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

For more information see:

 * [requests](https://github.com/kennethreitz/requests)
 * [requests-oauthlib](https://github.com/requests/requests-oauthlib)
 * [python-jose](https://pypi.org/project/python-jose/)

## <a name="config-files"></a> Config files

We have separated the default configuration out into its own file for the
majority of the examples.  This is the [config.default.json](config.default.json)
file.

Rather than overriding the values in there, we suggest you override only the
settings you need in the [config.json](config.json) file as both of these
files are imported into the script and the values in [config.json](config.json)
override those in [config.default.json](config.default.json).

As a minimum, you will need to set the following values in the `bc` section,
otherwise the scripts will not run:

* `webservice_key`
* `webservice_secret`
* `server_uri`

If you are using the [authenticate_jwt.py](authenticate_jwt.py) script you will
also need to include:
 * `jwt_service_user` - the username of one of the registered service users

and copy the private key created for JWT generation into the `jwt_private_key.pem`
file.  This file name is included in the default config, the value should be
changed if it is stored somewhere else.

An example of [config.json](config.json) configured for a development server
is as follows:

```json
{
    "bc": {
        "webservice_key": "ecb03487-b4fe-4e0b-9766-f40fbe01292b",
        "webservice_secret": "aefb-365f-9916-618e-5e61-5dae-9fbf-cc85",
        "server_uri": "https://dev.groupbc.com/",
        "service_user": "service_user"
    },
    "development": {
        "logging": {
            "response_json": true
        }
    }
}
```

This is a JSON file, so if the file is not valid JSON then the scripts will
not run.

## <a name="rest-and-discoverability"></a> REST and Discoverability

REST, or REpresentational State Transfer, is an architectural style, but when
applied to a Web Service, it refers to a communication protocol that:

1. Uses standard HTTP methods (GET, PUT, POST or DELETE)
2. Calls these methods on resources using URIs
3. Uses standard Internet media types for the communication (XML, JSON, etc)
4. Uses hypertext links for related resources
5. Is stateless

Looking at this in relation to how we interact with the Web Services API:

1. The OAuth methods all make HTTP requests. To get a container listing, we
    make a GET request. Adding a document is done using a POST request.
2. The endpoints are all URIs.  The following are two examples:
    *   `https://dev.groupbc.com/api/services/lookup_uri`
    *   `https://dev.groupbc.com/api/artifacts/project-694`
3. The parameters passed to these URIs, e.g, name, content_type, content and
    kind when [Creating a Document](../guides/create_document.md), are passed
    as a JSON object.
4. Discovering the URI of, for example, a folder's contents is done by starting
    at the API root, `/api/`, and finding the appropriate related resource via
    hypertext links.
5. Each of the client requests contains complete authentication information as
    the server does not store session state. This information is sent as part
    of the HTTP headers in the OAuth methods.

As a result of this, the BCDE Web Services API can be referred to as RESTful,
that is, it implements a RESTful communication protocol.

One useful aspect of a RESTful API is discoverability.  From the root, we are
able to discover all the endpoints available to us.  As a result, in
the majority of our examples, we do not _construct_ URIs, but rather navigate
the keys that come back from each request and use the values we find there.

We will not change the existing keys in the published version of our API.
What may change are the values those keys point to that define our endpoints.

If you construct URIs in your application, were endpoints to change in a later
version of BCDE, then these functions would fail, whereas a discoverable approach
that looks up the values of the endpoints would not.

There is an overhead to taking a discoverable approach in your application,
however, in terms of making multiple calls to discover an endpoint when a
potential URI is obvious (e.g. https://dev.groupbc.com/api/artifacts/project-694
for a Project in BCDE that has a BCDE ID of `694`).  There is a technique using the
EXPAND parameter that can be employed to minimise the number of calls, so this
should be considered over the construction of URIs.

See [Expand](expand.py) for more an example of this.
