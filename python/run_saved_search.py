"""
Example to show running a saved search in BCDE
Compatible with Python 3.9
"""

import json
import ws_utils

# Get the config
CONFIG = ws_utils.get_config()
# Enable logging
LOG_RESPONSE_JSON = CONFIG['development']['logging']['response_json']
LOG_FULL_HTTP_DEBUG = CONFIG['development']['logging']['full_http_debug']
if LOG_FULL_HTTP_DEBUG:
    ws_utils.enable_logging()
# Authenticate and get back the BCDE authenticated OAuth session
bc_session = ws_utils.authenticate(CONFIG)
bc_session.verify = CONFIG['development']['verify_ssl']

# Respect the rate limiter and retry requests that return a 429 error.
ws_utils.rate_limit_requests(bc_session)

# For this example we will assume we already have the BCDE ID for the saved
# search artifact that we want to run
SAVED_SEARCH_ARTIFACT_ID = CONFIG['demo']['run_saved_search']['saved_search_id']
if not SAVED_SEARCH_ARTIFACT_ID:
    raise RuntimeError("You must set the 'saved_search_id' parameter to the "
                       " ID of saved search you have access to in config.json "
                       "in order to run this example")

# Get the root endpoint to identify the services
root_uri = ws_utils.get_uri('/api/')
response = bc_session.get(
    root_uri
)
# Raise an error if we don't get back a 200
response.raise_for_status()
root = response.json()
ws_utils.log('GET', root_uri, response)

returned_lookup_uri = root.get('services', {}).get('lookup_uri', None)
if returned_lookup_uri is None:
    raise RuntimeError("It has not been possible to get the lookup_uri "
                       "endpoint that is needed to resolve the URI for "
                       "the Saved Search.")

lookup_uri = ws_utils.get_uri(returned_lookup_uri)

# Given an ID of an artifact in BCDE, lookup its API URI.
params = [SAVED_SEARCH_ARTIFACT_ID,]
response = bc_session.post(
    lookup_uri,
    json=params
)
# Raise an error if we don't get back a 200
response.raise_for_status()
uris = response.json()
ws_utils.log('POST', lookup_uri, response, params)

# The lookup_uri endpoint returns each URI keyed by artifact ID.  These IDs
# are strings, so we need to cast our artifact ID as a string.
artifact_uri = ws_utils.get_uri(uris[str(SAVED_SEARCH_ARTIFACT_ID)])
response = bc_session.get(
    artifact_uri
)
# Raise an error if we don't get back a 200
response.raise_for_status()
artifact = response.json()
ws_utils.log('GET', artifact_uri, response)

# Saved searches are special.  For container artifacts, 'contents' tells us
# about the things they actually contain, but the contents of a saved search
# (which doesn't really have contents) is interpreted specially by the server
# and causes the search to be run.
returned_contents =  artifact.get('contents', None)
if returned_contents is None:
    raise RuntimeError("It has not been possible to get the contents "
                       "key that contains the URI of the Saved Search "
                       "contents endpoint.")

artifact_contents_uri = ws_utils.get_uri(returned_contents)

response = bc_session.get(
    artifact_contents_uri
)
# Raise an error if we don't get back a 200
response.raise_for_status()
artifact_contents = response.json()
ws_utils.log('GET', artifact_contents_uri, response)

# Check to see if the total is an exact count or an approximation
is_exact_string = ''
if not artifact_contents.get("is_total_exact"):
    is_exact_string = ' about'
# Check to see if there are more pages of results available
more_pages_available_string = ''
if artifact_contents.get("next"):
    more_pages_available_string = '  More pages available.'

print("\n\nSaved search on %s" % (artifact_contents_uri, ))
# Show the number of results
print("Found%s %d result(s).%s" % (
    is_exact_string,
    artifact_contents.get("total", 0),
    more_pages_available_string,
))
# List out the results showing in this page of
print("Showing %d result(s):" % (len(artifact_contents.get("contents", [])), ))
for index, content in enumerate(artifact_contents.get("contents", [])):
    print(" * %d. %s : %s" % (
        index + 1,
        content.get("name", ""),
        content.get("uri", "")
    ))
