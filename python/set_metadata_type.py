"""Compatible with Python 3.9"""

import urllib.request, urllib.parse, urllib.error
import json
import ws_utils

# Get the config
CONFIG = ws_utils.get_config()
# Enable logging
LOG_RESPONSE_JSON = CONFIG['development']['logging']['response_json']
LOG_FULL_HTTP_DEBUG = CONFIG['development']['logging']['full_http_debug']
if LOG_FULL_HTTP_DEBUG:
    ws_utils.enable_logging()

# Authenticate and get back the BCDE authenticated OAuth session
bc_session = ws_utils.authenticate(CONFIG)
bc_session.verify = CONFIG['development']['verify_ssl']

# Respect the rate limiter and retry requests that return a 429 error.
ws_utils.rate_limit_requests(bc_session)

ARTIFACT_ID = CONFIG['demo']['set_metadata_type']['artifact_id']
if not ARTIFACT_ID:
    raise RuntimeError("You must set the 'artifact_id' parameter to the ID of "
                       "an artifact you have access to in config.json "
                       "in order to run this example")
METADATA_TYPE_NAME = CONFIG['demo']['set_metadata_type']['type_name']

# Get the root endpoint to identify the services
root_uri =  ws_utils.get_uri('/api/')
response = bc_session.get(
   root_uri
)
# Raise an error if we don't get back a 200
response.raise_for_status()
root = response.json()
ws_utils.log('GET', root_uri, response)


lookup_uri = ws_utils.get_uri(root["services"]["lookup_uri"])

# Given an ID of an artifact in BCDE, lookup its API URI.
params = [ARTIFACT_ID,]
response = bc_session.post(
    lookup_uri,
    json=params,
)
# Raise an error if we don't get back a 200
response.raise_for_status()
uris = response.json()
ws_utils.log('POST', lookup_uri, response, params)

artifact_uri = ws_utils.get_uri(uris[str(ARTIFACT_ID)])
response = bc_session.get(
    "%s" % (artifact_uri, )
)
# Raise an error if we don't get back a 200
response.raise_for_status()
artifact = response.json()
ws_utils.log('GET', artifact_uri, response)

artifact_assignable_types_uri = ws_utils.get_uri(artifact["assignable_types"])
artifact_type_uri = ws_utils.get_uri(artifact["type"])

# Get all assignable types in the artifact
response = bc_session.get(
    artifact_assignable_types_uri
)
# Raise an error if we don't get back a 200
response.raise_for_status()
assignable_types = response.json()
ws_utils.log('GET', artifact_assignable_types_uri, response)

# Get the assignable types.  See the documentation on your BCDE server at the
# following path for more information about this endpoint:
#   /bc/bc.cgi?op=web_service_documentation&resource=AssignableTypesResource
type_id = None
for type in assignable_types['types']:
    if type['name'] == METADATA_TYPE_NAME:
        type_id = type['id']

# Set the metadata type for the artifact.  This is done by doing a HTTP PUT
# to the type endpoint, passing through the URI for the type (which is accessed
# in the 'id' key) as the body of the request.  See the documentation on your
# BCDE server at the following path for more information about this endpoint:
#    /bc/bc.cgi?op=web_service_documentation&resource=MetadataTypeResource
response = bc_session.put(
    artifact_type_uri,
    json=type_id
)
# Raise an error if we don't get back a 200
response.raise_for_status()
set_type_result = response.json()
ws_utils.log('PUT', artifact_type_uri, response)

# The endpoint returns NULL so there is nothing to check against.
print("\n\nType '%s' set on %s" % (type_id, artifact_uri, ))
