#!/usr/bin/python3.9
# (c) 2019-2024 Bentley Systems, Incorporated. All rights reserved.
import base64
import io
import json
import math
import os
import pprint
import sys
import tempfile
import uuid
import ws_utils


config = ws_utils.get_config()
server_url = ws_utils.trim_server_uri(config["bc"]["server_uri"])
VERIFY = config['development']['verify_ssl']

# If True, this will not use the Base64FileWrapper, FileConcatenator,
# data_concatenator classes and functions below, but create a temporary JSON
# file on disk containing the whole JSON payload. This implementation is
# simpler, but requires (potentially significant) temporary disk space.
CREATE_TEMP_FILE_ON_DISK = False

# Given a file object, this creates a file-like wrapper object which emits the
# base64 encoded file contents on demand.
class Base64FileWrapper(object):
    def __init__(self, file):
        self._file = file
        self._base64_buffer = b''

    def read(self, size):
        if size < 0:
            raise ValueError('size must be non-negative')

        # If the base64_buffer is low, fill it up
        if len(self._base64_buffer) < size:
            further_bytes_required = size - len(self._base64_buffer)
            # We must read a multiple of 3 bytes from the file for it to be
            # base64 encoded without padding. Padding is only acceptable at
            # the end of the file.
            to_read = int(math.ceil(further_bytes_required / 4)) * 3
            data = self._file.read(to_read)
            # We use base64.b64encode so white-space is not introduced.
            self._base64_buffer += base64.b64encode(data)

        return_data = self._base64_buffer[:size]
        self._base64_buffer = self._base64_buffer[size:]
        return return_data


# Give a list of file objects, the .read() method of this class will return
# data from each file in order until the last file has been exhausted.
class FileConcatenator:
    def __init__(self, files):
        self._g = self._generator(files)

    def _generator(self, files):
        for f in files:
            while d := f.read(self._size if self._size != -1 else 32768):
                yield d

    def read(self, size=-1):
        self._size = size
        try:
            return next(self._g)
        except StopIteration:
            return ''


# Given a list of strings and binary file objects, this returns a file-like
# object concatenating them. Strings will be UTF-8 encoded. File contents are
# base64 encoded. This also returns the overall size, in bytes, of the
# concatenated data.
def data_concatenator(items):
    files = []
    size = 0
    for item in items:
        if isinstance(item, str):
            # Convert the bytes, to a file-like object containing those bytes.
            item_bytes = item.encode('utf-8')
            files.append(io.BytesIO(item_bytes))
            size += len(item_bytes)
        elif isinstance(item, io.IOBase) and not isinstance(item, io.TextIOBase):
            # Wrap the binary file with the base64 encoder. This doesn't need
            # any JSON escaping as the base64 content does not require it.
            files.append(Base64FileWrapper(item))
            size += int(4 * math.ceil(os.path.getsize(item.name) / 3.0))
        else:
            raise ValueError(
                'Expected bytes or open binary file, got %s' % (type(item),)
            )

    return size, FileConcatenator(files)


def main(argv):
    try:
        filepath = argv[1]
    except IndexError:
        sys.stdout.write('Please provide the path of a file to upload\n\n')
        sys.exit(1)

    if not os.path.exists(filepath):
        sys.stdout.write('%s does not exist' % (filepath,))
        sys.exit(1)

    if not os.path.isfile(filepath):
        sys.stdout.write('%s is not a file' % (filepath,))
        sys.exit(1)

    bc_session = ws_utils.authenticate(config)

    # If this request succeeds then we should be authenticated.
    response = bc_session.get('%s/api/' % server_url, verify=VERIFY)

    # Construct the URL of the current user's home's contents so we can use
    # that as the target for the upload.
    home_contents_url = server_url + response.json()['home'] + '/contents'

    separator = str(uuid.uuid4())

    document_create_payload = {
        'name': os.path.basename(filepath),
        # The content_type should be more appropriately set to reflect the MIME
        # type of the content of the file.
        'content_type': 'application/octet-stream',
        'content_encoding': 'base64',
        # The separator for content is just a unique string which allows us to
        # split the JSON body into two parts with the file delivery inserted
        # between.
        'content': separator,
        'kind': 'document',
    }

    # Take the above JSON payload we've just created and split it into the part
    # before the file contents and the part after the file contents.
    try:
        (
            document_create_payload_json_beginning,
            document_create_payload_json_end,
        ) = json.dumps(document_create_payload).split(separator)
    except ValueError:
        sys.stderr.write('Payload could not be split into two pieces')
        sys.exit(1)

    if CREATE_TEMP_FILE_ON_DISK:
        # Open a  temporary file for reading and writing in binary mode.
        with tempfile.TemporaryFile(mode='w+b') as tmp:
            tmp.write(document_create_payload_json_beginning.encode('utf-8'))
            with open(filepath, 'rb') as payload:
                # This MUST be a multiple of 3 because every 3 bytes of data
                # becomes 4 bytes when base64 encoded and we don't want the
                # termination padding until the end of the file is reached.
                while payload_chunk := payload.read(3_000_000):
                    tmp.write(base64.b64encode(payload_chunk))
            tmp.write(document_create_payload_json_end.encode('utf-8'))
            tmp.seek(0)

            response = bc.post(
                home_contents_url,
                headers={
                    'Content-Type': 'application/json',
                    # A Content-Length header will be added automatically here.
                },
                data=tmp,
            )

    else:
        # Create the file-like object consisting of the first part of the JSON
        # structure, the file, and then the end of the JSON structure. The
        # strings will be UTF-8 encoded and the file will be base64 encoded by
        # the data_concatenator.
        size, data_generator = data_concatenator(
            [
                document_create_payload_json_beginning,
                open(filepath, 'rb'),
                document_create_payload_json_end,
            ]
        )

        # Post the data using the requests library which will .read() all the data
        # from the data_generator and send it to the server.
        response = bc_session.post(
            home_contents_url,
            headers={
                'Content-Type': 'application/json',
                'Content-Length': str(size),
            },
            data=data_generator,
        )

    pprint.pprint(response.json())


if __name__ == '__main__':
    main(sys.argv)
