"""Compatible with Python 3.9"""

from typing import Optional, Any

import json
import os
import requests
import requests_oauthlib
import sys
import time
import urllib.parse
import webbrowser


def get_path() -> str:
    this_file = sys.argv[0] if __name__ == '__main__' else __file__
    return os.path.split(os.path.abspath(this_file))[0]


def get_config() -> dict[str, Any]:
    """Get the shared config for the applications.  This reads the config
    from the config.default.json file and overwrites the default values
    with anything that has been provided in config.json
    """
    path = get_path()

    config = {}

    with open(
        os.path.join(path, 'config.default.json'),
        mode='rt',
        encoding='utf-8',
    ) as file:
        update(config, json.load(file))

    with open(
        os.path.join(path, 'config.json'),
        mode='rt',
        encoding='utf-8',
    ) as file:
        update(config, json.load(file))

    required_oauth_settings_default_values = {
        "webservice_key": "<OAUTH_KEY>",
        "webservice_secret": "<OAUTH_SECRET>",
        "server_uri": "<SERVER_BASE_URL>",
    }

    for key, default_value in list(required_oauth_settings_default_values.items()):
        if config['bc'][key] == default_value:
            raise RuntimeError(
                "You must set an actual value for the config setting: '%s'. "
                "It cannot be left with the default value '%s'"
                % (
                    key,
                    default_value,
                )
            )

    return config


# Like dict.update, but does recursive updates.
def update(d: dict[Any, Any], u: dict[Any, Any]) -> dict[Any, Any]:
    for k, v in u.items():
        d[k] = update(d.get(k, {}), v) if isinstance(v, dict) else v
    return d


def trim_server_uri(server_uri: str) -> str:
    # Remove trailing slashes from the base else we'll double up on joining
    # (and create an invalid oauth signature).
    return server_uri.rstrip('/')


def authentication_token_filepath() -> str:
    config = get_config()
    path = get_path()

    return os.path.join(path, config['bc']['token_file_name'])


def save_authentication_token(token: dict[str, str]):
    """Function to save the authentication token to disk so it
    can be reused.
    """

    file_path = authentication_token_filepath()
    with open(file_path, mode='wt', encoding='utf-8') as file:
        json.dump(token, file, indent=4)


def load_authentication_token() -> dict[str, str]:
    """Function to load a saved authentication token from disk if it
    exists and return it
    """

    token = None
    file_path = authentication_token_filepath()
    if os.path.isfile(file_path):
        with open(file_path, mode='rt', encoding='utf-8') as file:
            token = json.load(file)
    return token


def authenticate(config: dict[str, Any]) -> requests_oauthlib.OAuth1Session:
    """Function to authenticate with BCDE using the OAuth information
    stored in the passed in config and return an authenticated OAuth
    session to BCDE
    """

    # Set the key, secret and callback values from config
    webservice_key = config['bc']['webservice_key']
    webservice_secret = config['bc']['webservice_secret']
    oauth_callback_uri = config['bc']['oauth_callback_uri']
    server_uri = trim_server_uri(config['bc']['server_uri'])

    bc_session = requests_oauthlib.OAuth1Session(
        webservice_key,
        client_secret=webservice_secret,
        callback_uri=oauth_callback_uri,
    )

    # Try to load the authenation token
    token = load_authentication_token()
    if token is not None:
        # We have loaded an existing token, so try to update bc_session.token.
        # The token attribute was only added to OAuth1Session in version 1.0.0
        # of the requests-oauthlib library, so if you are using version 0.8.0
        # you will not be able to create a new OAuth1Session and set the
        # access_token that you have previously saved.
        if hasattr(bc_session, 'token'):
            bc_session.token = token

        response = bc_session.get(
            '%s/api/' % server_uri,
            verify=config['development']['verify_ssl']
        )

        # If the token that's stored is valid, return the bc_session
        if response.status_code == 200:
            return bc_session

    # If we get here, we don't have a valid token, so fetch and authorise
    # Clear the invalid token
    bc_session.token = {"oauth_token_secret": None, "oauth_token": None}
    # Fetch a new request token
    try:
        bc_session.fetch_request_token(
            "%s/oauth/request_token" % server_uri,
            verify=config['development']['verify_ssl'],
        )
    except requests.exceptions.SSLError as e:
        raise RuntimeError("The requests.exceptions.SSLError has been raised. "
                           "If you are running this demo code on a "
                           "development server, you might want to set the "
                           "config setting 'verify_ssl' to false.  You should "
                           "NOT do this in production. "
                           "Full error follows:\n%s" % str(e))

    auth_url = bc_session.authorization_url(
        "%s/oauth/authorise" % server_uri,
    )

    # In a real application, rather than a command line example, this is the
    # point where your app would redirect them to the OAuth login, get them
    # to authorise this application to do things on their behalf, and, by
    # providing the oauth_callback, redirect them back to your application,
    # which will include the verifier parameter

    # Here we simply get their browser to open up the URL.
    print("Attempting to open %s in your browser.  Copy-and-paste this URL " \
          "into your browser if this doesn't happen automatically." \
          % (auth_url,))
    webbrowser.open(auth_url)

    # We can enable a verifier by registering the application as using a
    # verifier.  If we do, then that will be displayed on the authorise screen
    # after the user has authorised as well as being included in the auth
    # redirection url.  However, this is only fully implemented from from
    # BCDE 7.2.2 onwards and only if the use_verifier_token=yes has been
    # supplied when registering the application in.
    # We need to provide a non-empty string for verifier.

    redirect_response = input(
        "Paste the full redirect URL here once you have logged in " +
        "and press enter..."
    )

    authorisation_response_parsed = bc_session.parse_authorization_response(
        redirect_response
    )

    # Check to make sure the verifier is passed through.
    verifier = "admin"
    if "oauth_verifier" in authorisation_response_parsed:
        verifier = authorisation_response_parsed["oauth_verifier"]

    access_token = bc_session.fetch_access_token(
        "%s/oauth/access_token" % server_uri,
        verifier=verifier
    )
    save_authentication_token(access_token)

    return bc_session


def log(
    method: str,
    uri: str,
    response: requests.Response,
    params: Optional[dict[str, str]] = None,
):
    config = get_config()
    log_response_json = config['development']['logging']['response_json']

    if log_response_json:
        response_json = response.json()
        print("-----------")
        print("\nHTTP response: %s\n" % response.status_code)
        if params is None:
            print("%s of %s returned:" % (method, uri,))
        elif isinstance(params, dict):
            print("%s of %s with the following params " % (method, uri,))
            if not params:
                print("    {}")
            else:
                for key, value in sorted(params.items()):
                    print("    \"%s\": \"%s\"" % (key, value, ))
            print("returned:")
        else:
            print("%s of %s with the following params " % (method, uri,))
            if not params:
                print("    []")
            else:
                for value in params:
                    print("    %s" % (value, ))
            print("returned:")
        print(json.dumps(response_json, indent=4, sort_keys=True))
        print("-----------")


def rate_limit_requests(bc_session: requests_oauthlib.OAuth1Session):

    # NOTE: We cannot use the urllib3 Retry class as that wont regenerate the
    # authentication header for each request.
    class RetryHTTPAdapater(requests.adapters.HTTPAdapter):

        def __init__(self, auth: requests_oauthlib.OAuth1):
            super().__init__()
            self.auth = auth

        @staticmethod
        def wait_for_retry_after_seconds(response: requests.Response):
            try:
                retry_after = int(response.headers.get('Retry-After'))
            except (TypeError, ValueError):
                retry_after = 30
            time.sleep(retry_after)

        def refresh_authentication_header(self, request: requests.PreparedRequest):
            request.prepare_auth(self.auth)

        def send(self, request: requests.PreparedRequest, *args, **kwargs) -> requests.Response:
            response = None
            # Limit the number of retries so we don't get stuck forever.
            retries = 10

            while retries and (response is None or response.status_code in (409, 429)):
                if response is not None:
                    self.wait_for_retry_after_seconds(response)
                    self.refresh_authentication_header(request)

                response = super().send(request, *args, **kwargs)
                retries -= 1

            return response

    adapter = RetryHTTPAdapater(bc_session.auth)

    bc_session.mount('http://', adapter)
    bc_session.mount('https://', adapter)


def enable_logging():
    import http.client as http_client
    import logging

    # Setup the logging
    http_client.HTTPConnection.debuglevel = 1
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True


def get_uri(path: str, params: Optional[dict[str, str]] = None) -> str:
    import urllib.request, urllib.parse, urllib.error

    config = get_config()
    parts = list(urllib.parse.urlsplit(config['bc']['server_uri']))
    parts[2] = path
    if params:
        parts[3] = urllib.parse.urlencode(params)
    return urllib.parse.urlunsplit(parts)
