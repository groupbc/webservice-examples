"""
Example of searching for a document in BCDE and get the latest version of it
Compatible with Python 3.9
"""

import json
import ws_utils

# Get the config
CONFIG = {
    "bc": {
        "webservice_key": "<OAUTH_KEY>",
        "webservice_secret": "<OAUTH_SECRET>",
        "server_uri": "<SERVER_BASE_URL>",
        "oauth_callback_uri": "http://127.0.0.1:5000/oauth_app/authorised"
    },
    "development": {
        "verify_ssl": True,
        "logging": {
            "response_json": False,
            "full_http_debug": False,
        }
    },
    "demo": {
        "document_name": 'Higher Education Fact Sheet To Be Downloaded.pdf',
        "downloaded_name": 'Higher Education Fact Sheet - Downloaded.pdf',
    }
}
# If the key is not found, a request to Authenticate in BCDE will
# raise a KeyError

# Enable logging
LOG_RESPONSE_JSON = CONFIG['development']['logging']['response_json']
LOG_FULL_HTTP_DEBUG = CONFIG['development']['logging']['full_http_debug']
if LOG_FULL_HTTP_DEBUG:
    ws_utils.enable_logging()
# Authenticate and get back the BCDE authenticated OAuth session
print("Authenticating...")
bc_session = ws_utils.authenticate(CONFIG)
print("Authenticated")
bc_session.verify = False

# Respect the rate limiter and retry requests that return a 429 error.
ws_utils.rate_limit_requests(bc_session)

DOCUMENT_NAME = CONFIG['demo']['document_name']
DOWNLOADED_NAME = CONFIG['demo']['downloaded_name']

# Get the user's home which is stored in the /api/ endpoint
root_uri = ws_utils.get_uri('/api/')
response = bc_session.get(
    root_uri
)
# Raise an error if we don't get back a 200
response.raise_for_status()
root = response.json()
ws_utils.log('GET', root_uri, response)

if root.get('services', '') != '' \
        and root.get('services').get('simple_search'):
    search_uri = ws_utils.get_uri(root.get('services').get('simple_search'))

else:
    raise RuntimeError('The root endpoint did not return a simple_search ')

# Construct a search using BCDE's Keyword language.  See section 6.7 of the
# help for information on how to use this.
# Here we look for a document with a defined name in the user's home,
# matching against latest versions of documents only.
params = {
     'location': root.get('home', ''),
     'klang': "(name:%s) and (class:%s)" % (
         DOCUMENT_NAME,
         'document'
     ),
     'refresh_if_older': 1,
     'revisions': 'latest_only',
 }
response = bc_session.get(
    search_uri,
    params = params,
)
# Raise an error if we don't get back a 200
response.raise_for_status()
found_items = response.json()
ws_utils.log('GET', search_uri, response, params)

# Get the first matching item, if one exists
if len(found_items.get('contents', [])) == 0:
    print("\n\nNo documents found with the name '%s'." % (DOCUMENT_NAME,))
else:
    # Get the contents index of the items returned, look at the first
    # item, then get its uri endpoint
    contents_uri = ws_utils.get_uri(found_items.get('contents', [])[0].get('uri', ''))

    # Get the information about the first file returned
    response = bc_session.get(
        contents_uri
    )
    # Raise an error if we don't get back a 200
    response.raise_for_status()
    latest_version = response.json()
    ws_utils.log('GET', contents_uri, response)

    latest_version_file_uri = ws_utils.get_uri(latest_version.get('file', ''))

    # Get the Document File Resource endpoint
    print(("Downloading file '%s'..." % DOCUMENT_NAME))
    latest_version_file_response = bc_session.get(
        latest_version_file_uri
    )
    # Raise an error if we don't get back a 200
    latest_version_file_response.raise_for_status()

    # For more information about this endpoint, go to the following URL on a
    # BCDE server
    #    /bc/bc.cgi?op=web_service_documentation&resource=DocumentFileResource

    # The response object has the stream of the document data, which doesn't
    # have a JSON representation, so we use custom logging to output the
    # response headers instead.
    if LOG_RESPONSE_JSON:
        print("-----------")
        print("%s returned the following headers:" % (latest_version_file_uri,))
        print(latest_version_file_response.headers)
        print("-----------")

    # Write the content returned as binary to a file
    new_file = open(DOWNLOADED_NAME, 'wb')
    new_file.write(latest_version_file_response.content)
    new_file.close()

    print("Downloaded file '%s'.  Saved as local file '%s'" % (
        DOCUMENT_NAME,
        DOWNLOADED_NAME
    ))
