GET of https://centos7/api/ returned:
{
    "artifacts": "/api/artifacts",
    "bag": "/api/artifacts/bag-508",
    "current_state": {
        "time": "2019-07-04T12:40:40.076588+00:00",
        "user": "/api/artifacts/user-492"
    },
    "experimental": {
        "templates": "/api/templates/experimental"
    },
    "home": "/api/artifacts/home-497",
    "metadata_datatypes": "/api/metadata_datatypes",
    "schema_bag": "/api/artifacts/metadatabag-563",
    "server_logo": "/api/server_logo",
    "services": {
        "lookup_id": "/api/services/lookup_id",
        "lookup_uri": "/api/services/lookup_uri",
        "lookup_user_addressbook": "/api/services/lookup_user_addressbook",
        "simple_search": "/api/services/simple_search?"
    },
    "waste": "/api/artifacts/waste-503"
}

GET of https://centos7/api/services/simple_search? with the following params
    "klang": "md_3163='Accordion z-fold'"
returned:
{
    "contents": [
        {
            "created": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-06-19T12:35:49.282220+00:00",
                "user": "/api/artifacts/user-492"
            },
            "descr": "",
            "icon": {
                "default": {
                    "alt_text": "Plain text",
                    "height": 21,
                    "href": "https://centos7/bscw_icons/text.gif",
                    "width": 21
                }
            },
            "kind": "document",
            "mime_label": "Plain text",
            "mime_type": "text/plain",
            "modified": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-06-25T07:40:32.159498+00:00",
                "user": "/api/artifacts/user-492"
            },
            "name": "Document for WD Issue 1.txt",
            "uri": "/api/artifacts/document-12623",
            "versioning": {},
            "web_ui_link_get": {
                "can_perform": true,
                "name": "get",
                "requires_entry": false,
                "title": "Get",
                "url": "https://centos7/bc/bc.cgi/d12623/Document%20for%20WD%20Issue%201.txt"
            },
            "web_ui_link_lp": {
                "can_perform": true,
                "name": "lp",
                "requires_entry": false,
                "title": "lp",
                "url": "https://centos7/bc/bc.cgi/0/694?op=lp&id=12623"
            }
        },
        {
            "created": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-05-22T11:10:25.302290+00:00",
                "user": "/api/artifacts/user-492"
            },
            "descr": "",
            "icon": {
                "default": {
                    "alt_text": "Plain text",
                    "height": 21,
                    "href": "https://centos7/bscw_icons/text.gif",
                    "width": 21
                }
            },
            "kind": "document",
            "mime_label": "Plain text",
            "mime_type": "text/plain",
            "modified": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-06-25T14:52:59.507904+00:00",
                "user": "/api/artifacts/user-492"
            },
            "name": "Document for WD Issue 10.txt",
            "uri": "/api/artifacts/document-12061",
            "versioning": {},
            "web_ui_link_get": {
                "can_perform": true,
                "name": "get",
                "requires_entry": false,
                "title": "Get",
                "url": "https://centos7/bc/bc.cgi/d12061/Document%20for%20WD%20Issue%2010.txt"
            },
            "web_ui_link_lp": {
                "can_perform": true,
                "name": "lp",
                "requires_entry": false,
                "title": "lp",
                "url": "https://centos7/bc/bc.cgi/0/694?op=lp&id=12061"
            }
        },
        {
            "created": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-05-22T11:02:20.840784+00:00",
                "user": "/api/artifacts/user-492"
            },
            "descr": "",
            "icon": {
                "default": {
                    "alt_text": "Plain text",
                    "height": 21,
                    "href": "https://centos7/bscw_icons/text.gif",
                    "width": 21
                }
            },
            "kind": "document",
            "mime_label": "Plain text",
            "mime_type": "text/plain",
            "modified": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-06-27T15:04:35.487272+00:00",
                "user": "/api/artifacts/user-492"
            },
            "name": "Document for WD Issue 12.txt",
            "uri": "/api/artifacts/document-12043",
            "versioning": {},
            "web_ui_link_get": {
                "can_perform": true,
                "name": "get",
                "requires_entry": false,
                "title": "Get",
                "url": "https://centos7/bc/bc.cgi/d12043/Document%20for%20WD%20Issue%2012.txt"
            },
            "web_ui_link_lp": {
                "can_perform": true,
                "name": "lp",
                "requires_entry": false,
                "title": "lp",
                "url": "https://centos7/bc/bc.cgi/0/694?op=lp&id=12043"
            }
        },
        {
            "created": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-05-07T09:13:05.810670+00:00",
                "user": "/api/artifacts/user-492"
            },
            "descr": "!!!!",
            "icon": {
                "default": {
                    "alt_text": "Plain text",
                    "height": 21,
                    "href": "https://centos7/bscw_icons/text.gif",
                    "width": 21
                }
            },
            "kind": "document",
            "mime_label": "Plain text",
            "mime_type": "text/plain",
            "modified": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-06-27T15:13:21.401171+00:00",
                "user": "/api/artifacts/user-492"
            },
            "name": "Document for WD Issue 14.txt",
            "uri": "/api/artifacts/document-10548",
            "versioning": {},
            "web_ui_link_get": {
                "can_perform": true,
                "name": "get",
                "requires_entry": false,
                "title": "Get",
                "url": "https://centos7/bc/bc.cgi/d10548/Document%20for%20WD%20Issue%2014.txt"
            },
            "web_ui_link_lp": {
                "can_perform": true,
                "name": "lp",
                "requires_entry": false,
                "title": "lp",
                "url": "https://centos7/bc/bc.cgi/0/694?op=lp&id=10548"
            }
        },
        {
            "created": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-05-02T18:27:54.546544+00:00",
                "user": "/api/artifacts/user-492"
            },
            "descr": "!!",
            "icon": {
                "default": {
                    "alt_text": "Plain text",
                    "height": 21,
                    "href": "https://centos7/bscw_icons/text.gif",
                    "width": 21
                }
            },
            "kind": "document",
            "mime_label": "Plain text",
            "mime_type": "text/plain",
            "modified": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-06-27T15:21:21.101924+00:00",
                "user": "/api/artifacts/user-492"
            },
            "name": "Document for WD Issue 15.txt",
            "uri": "/api/artifacts/document-10532",
            "versioning": {},
            "web_ui_link_get": {
                "can_perform": true,
                "name": "get",
                "requires_entry": false,
                "title": "Get",
                "url": "https://centos7/bc/bc.cgi/d10532/Document%20for%20WD%20Issue%2015.txt"
            },
            "web_ui_link_lp": {
                "can_perform": true,
                "name": "lp",
                "requires_entry": false,
                "title": "lp",
                "url": "https://centos7/bc/bc.cgi/0/694?op=lp&id=10532"
            }
        },
        {
            "created": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-05-02T18:27:37.207733+00:00",
                "user": "/api/artifacts/user-492"
            },
            "descr": "",
            "icon": {
                "default": {
                    "alt_text": "Plain text",
                    "height": 21,
                    "href": "https://centos7/bscw_icons/text.gif",
                    "width": 21
                }
            },
            "kind": "document",
            "mime_label": "Plain text",
            "mime_type": "text/plain",
            "modified": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-06-27T15:28:40.205571+00:00",
                "user": "/api/artifacts/user-492"
            },
            "name": "Document for WD Issue 16.txt",
            "uri": "/api/artifacts/document-10525",
            "versioning": {},
            "web_ui_link_get": {
                "can_perform": true,
                "name": "get",
                "requires_entry": false,
                "title": "Get",
                "url": "https://centos7/bc/bc.cgi/d10525/Document%20for%20WD%20Issue%2016.txt"
            },
            "web_ui_link_lp": {
                "can_perform": true,
                "name": "lp",
                "requires_entry": false,
                "title": "lp",
                "url": "https://centos7/bc/bc.cgi/0/694?op=lp&id=10525"
            }
        },
        {
            "created": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-05-24T14:24:15.465791+00:00",
                "user": "/api/artifacts/user-492"
            },
            "descr": "",
            "icon": {
                "default": {
                    "alt_text": "Plain text",
                    "height": 21,
                    "href": "https://centos7/bscw_icons/text.gif",
                    "width": 21
                }
            },
            "kind": "document",
            "mime_label": "Plain text",
            "mime_type": "text/plain",
            "modified": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-06-25T11:51:35.070551+00:00",
                "user": "/api/artifacts/user-492"
            },
            "name": "Document for WD Issue 5.txt",
            "uri": "/api/artifacts/document-12348",
            "versioning": {},
            "web_ui_link_get": {
                "can_perform": true,
                "name": "get",
                "requires_entry": false,
                "title": "Get",
                "url": "https://centos7/bc/bc.cgi/d12348/Document%20for%20WD%20Issue%205.txt"
            },
            "web_ui_link_lp": {
                "can_perform": true,
                "name": "lp",
                "requires_entry": false,
                "title": "lp",
                "url": "https://centos7/bc/bc.cgi/0/694?op=lp&id=12348"
            }
        },
        {
            "created": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-05-22T11:14:16.551946+00:00",
                "user": "/api/artifacts/user-492"
            },
            "descr": "",
            "icon": {
                "default": {
                    "alt_text": "Plain text",
                    "height": 21,
                    "href": "https://centos7/bscw_icons/text.gif",
                    "width": 21
                }
            },
            "kind": "document",
            "mime_label": "Plain text",
            "mime_type": "text/plain",
            "modified": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-06-25T13:07:31.291891+00:00",
                "user": "/api/artifacts/user-492"
            },
            "name": "Document for WD Issue 6.txt",
            "uri": "/api/artifacts/document-12112",
            "versioning": {},
            "web_ui_link_get": {
                "can_perform": true,
                "name": "get",
                "requires_entry": false,
                "title": "Get",
                "url": "https://centos7/bc/bc.cgi/d12112/Document%20for%20WD%20Issue%206.txt"
            },
            "web_ui_link_lp": {
                "can_perform": true,
                "name": "lp",
                "requires_entry": false,
                "title": "lp",
                "url": "https://centos7/bc/bc.cgi/0/694?op=lp&id=12112"
            }
        },
        {
            "created": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-05-22T11:11:33.343612+00:00",
                "user": "/api/artifacts/user-492"
            },
            "descr": "",
            "icon": {
                "default": {
                    "alt_text": "Plain text",
                    "height": 21,
                    "href": "https://centos7/bscw_icons/text.gif",
                    "width": 21
                }
            },
            "kind": "document",
            "mime_label": "Plain text",
            "mime_type": "text/plain",
            "modified": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-06-25T14:35:47.587980+00:00",
                "user": "/api/artifacts/user-492"
            },
            "name": "Document for WD Issue 8.txt",
            "uri": "/api/artifacts/document-12087",
            "versioning": {},
            "web_ui_link_get": {
                "can_perform": true,
                "name": "get",
                "requires_entry": false,
                "title": "Get",
                "url": "https://centos7/bc/bc.cgi/d12087/Document%20for%20WD%20Issue%208.txt"
            },
            "web_ui_link_lp": {
                "can_perform": true,
                "name": "lp",
                "requires_entry": false,
                "title": "lp",
                "url": "https://centos7/bc/bc.cgi/0/694?op=lp&id=12087"
            }
        },
        {
            "created": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-06-27T16:31:33.571214+00:00",
                "user": "/api/artifacts/user-492"
            },
            "descr": "",
            "icon": {
                "default": {
                    "alt_text": "Plain text",
                    "height": 21,
                    "href": "https://centos7/bscw_icons/text.gif",
                    "width": 21
                }
            },
            "kind": "document",
            "mime_label": "Plain text",
            "mime_type": "text/plain",
            "modified": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-06-27T16:31:33.571214+00:00",
                "user": "/api/artifacts/user-492"
            },
            "name": "Document to be Issue to User A.txt",
            "uri": "/api/artifacts/document-14072",
            "versioning": {},
            "web_ui_link_get": {
                "can_perform": true,
                "name": "get",
                "requires_entry": false,
                "title": "Get",
                "url": "https://centos7/bc/bc.cgi/d14072/Document%20to%20be%20Issue%20to%20User%20A.txt"
            },
            "web_ui_link_lp": {
                "can_perform": true,
                "name": "lp",
                "requires_entry": false,
                "title": "lp",
                "url": "https://centos7/bc/bc.cgi/0/694?op=lp&id=14072"
            }
        },
        {
            "created": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-07-03T12:21:58.258272+00:00",
                "user": "/api/artifacts/user-492"
            },
            "descr": "",
            "icon": {
                "default": {
                    "alt_text": "Plain text",
                    "height": 21,
                    "href": "https://centos7/bscw_icons/text.gif",
                    "width": 21
                }
            },
            "kind": "document",
            "mime_label": "Plain text",
            "mime_type": "text/plain",
            "modified": {
                "full_name": "D'Administrator",
                "login_name": "admin",
                "time": "2019-07-03T12:21:58.258272+00:00",
                "user": "/api/artifacts/user-492"
            },
            "name": "test.txt",
            "uri": "/api/artifacts/document-14357",
            "versioning": {},
            "web_ui_link_get": {
                "can_perform": true,
                "name": "get",
                "requires_entry": false,
                "title": "Get",
                "url": "https://centos7/bc/bc.cgi/d14357/test.txt"
            },
            "web_ui_link_lp": {
                "can_perform": true,
                "name": "lp",
                "requires_entry": false,
                "title": "lp",
                "url": "https://centos7/bc/bc.cgi/0/694?op=lp&id=14357"
            }
        }
    ],
    "first": "/api/services/simple_search?page=1&klang=md_3163%3D%27Accordion+z-fold%27",
    "is_search": true,
    "is_total_exact": true,
    "limit": 25,
    "next": null,
    "previous": null,
    "query_last_run": "2019-07-04T12:32:26.905645+00:00",
    "query_refresh": "/api/services/simple_search?klang=md_3163%3D%27Accordion+z-fold%27&refresh_if_older=0",
    "sort_by": [
        [
            "container_and_name",
            true,
            null
        ],
        [
            "id",
            true,
            null
        ]
    ],
    "start": 0,
    "total": 11,
    "view_preference": "/api/artifacts/searchbag-951/view_preference"
}


Search on https://centos7/api/services/simple_search?
Found 11 result(s).
Showing 11 result(s):
 * 1. Document for WD Issue 1.txt : /api/artifacts/document-12623
 * 2. Document for WD Issue 10.txt : /api/artifacts/document-12061
 * 3. Document for WD Issue 12.txt : /api/artifacts/document-12043
 * 4. Document for WD Issue 14.txt : /api/artifacts/document-10548
 * 5. Document for WD Issue 15.txt : /api/artifacts/document-10532
 * 6. Document for WD Issue 16.txt : /api/artifacts/document-10525
 * 7. Document for WD Issue 5.txt : /api/artifacts/document-12348
 * 8. Document for WD Issue 6.txt : /api/artifacts/document-12112
 * 9. Document for WD Issue 8.txt : /api/artifacts/document-12087
 * 10. Document to be Issue to User A.txt : /api/artifacts/document-14072
 * 11. test.txt : /api/artifacts/document-14357
