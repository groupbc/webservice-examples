Generated JWT:
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJodHRwczovL2NlbnRvczcvIiwiaXNzIjoiYzU3MDM2YTgtYjMwNi00ZDU4LWIwMmQtYWMwNWZhMTNkNmJjIiwianRpIjoiODA1ZTA2NTMtNjVmNy00ZGZmLWIwYmMtOTVjMDMxMTIxZjI2IiwidXNlciI6ImFkbWluIiwiZXhwIjoxNjQ2Mzk4NDQ1LjUzNywibmJmIjoxNjQ2Mzk4NDE1LjUzNywic3ViIjoiU0VSVklDRS1VU0VSLVJFUVVFU1QifQ.e4jVUHBXF0k4xaeU7hTuQG7Us-7pwiEvADkg0FQejKc


GET of https://centos7/api/ returned
{
    "artifacts": "/api/artifacts", 
    "bag": "/api/artifacts/bag-399", 
    "company_bag": "/api/artifacts/companybag-457", 
    "current_state": {
        "is_admin": false,
        "time": "2022-03-04T12:53:36.293424+00:00", 
        "user": "/api/artifacts/user-383"
    }, 
    "experimental": {
        "templates": "/api/templates/experimental"
    }, 
    "home": "/api/artifacts/home-388", 
    "metadata_datatypes": "/api/metadata_datatypes", 
    "schema_bag": "/api/artifacts/metadatabag-448", 
    "server_friendly_name": "XXXMyDevServer", 
    "server_logo": "/api/server_logo", 
    "services": {
        "batch": "/api/services/batch", 
        "batch_deferred": "/api/services/batch_deferred", 
        "lookup_company": "/api/services/lookup_company", 
        "lookup_id": "/api/services/lookup_id", 
        "lookup_metafields": "/api/services/lookup_metafields", 
        "lookup_placeholder_name": "/api/services/lookup_placeholder_name", 
        "lookup_uri": "/api/services/lookup_uri", 
        "lookup_user_addressbook": "/api/services/lookup_user_addressbook", 
        "simple_search": "/api/services/simple_search?"
    }, 
    "template_bag": "/api/artifacts/templatebag-687", 
    "user_roles": "/api/user_roles", 
    "waste": "/api/artifacts/waste-394"
}