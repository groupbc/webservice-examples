"""
Example to show how to create a document in BCDE
Compatible with Python 3.9
"""

import json
import ws_utils

# Get the config
CONFIG = ws_utils.get_config()
# Enable logging
LOG_RESPONSE_JSON = CONFIG['development']['logging']['response_json']
LOG_FULL_HTTP_DEBUG = CONFIG['development']['logging']['full_http_debug']
if LOG_FULL_HTTP_DEBUG:
    ws_utils.enable_logging()
# Authenticate and get back the BCDE authenticated OAuth session
bc_session = ws_utils.authenticate(CONFIG)
bc_session.verify = CONFIG['development']['verify_ssl']

# Respect the rate limiter and retry requests that return a 429 error.
ws_utils.rate_limit_requests(bc_session)

# SPACE_ID must be the BCDE ID of the container in which you want the new document
# to live. The BCDE ID can be found through the interface where it will be the
# last path segment of the url to that container. Rather than using a fixed
# container you can also use a search to dynamically find containers (see
# revise_document.py or simple_search.py for search examples).
# This is set in the config
SPACE_ID = CONFIG['demo']['create_document']['space_id']
if not SPACE_ID:
    raise RuntimeError("You must set the 'space_id' parameter to the ID of a "
                       "container you have access to in config.json "
                       "in order to run this example")

# Get the root endpoint to identify the services
root_uri = ws_utils.get_uri('/api/')
response = bc_session.get(
    root_uri
)
# Raise an error if we don't get back a 200
response.raise_for_status()
root = response.json()
ws_utils.log('GET', root_uri, response)

returned_lookup_uri = root.get('services', {}).get('lookup_uri', None)
if returned_lookup_uri is None:
    raise RuntimeError("It has not been possible to get the lookup_uri "
                       "endpoint that is needed to resolve the URI for "
                       "the Space")

artifact_lookup_uri = ws_utils.get_uri(returned_lookup_uri)

# Given an ID of a container in BCDE, lookup its API URI.
params = [SPACE_ID,]
response = bc_session.post(
    artifact_lookup_uri,
    json=params
)
# Raise an error if we don't get back a 200
response.raise_for_status()
artifact_lookup = response.json()
ws_utils.log('POST', artifact_lookup_uri, response, params)

container_uri = ws_utils.get_uri(artifact_lookup[str(SPACE_ID)])
SERVER_URI = CONFIG['bc']['server_uri']
uris = response.json()

if LOG_RESPONSE_JSON:
    print("-----------")
    print("%s/api/services/lookup_uri returned:" % (SERVER_URI, ))
    print(json.dumps(uris, indent=4))
    print("-----------")

# POSTing to the /contents endpoint of a container will create a document in
# it. A new document will always be created. To revise a document you must POST
# to its /versions endpoint instead. See lookup_revise_document.py
contents_uri = container_uri + '/contents'

# Create a text file that contains the string in config 'content' by
# POSTing to the contents of a container to create a document there
params = {
    # The file name to be created
    'name': CONFIG['demo']['create_document']['name'],
    # The content type of the file we're creating.
    'content_type': 'text/plain',
    # The encoding of the file contents.
    'content_encoding': 'ascii',
    # The content to be posted. The encoding must match the 'content_encoding"
    # above.
    'content': CONFIG['demo']['create_document']['content'],
    # The "type" of artifact we're creating.  When creating a document this
    # needs to be set to the string 'document'.
    'kind': 'document',
}
response = bc_session.post(
    contents_uri,
    json=params,
)
# For more information about the "kind", go to the following URL on a BCDE server
#    /bc/bc.cgi?op=web_service_documentation&resource=ContentsResource
# Raise an error if we don't get back a 200
response.raise_for_status()

# The response will be the URI of the newly created document
ws_utils.log("POST", contents_uri, response, params)

print("\n\nThe following document has been created in space %s:" % (container_uri, ))
print(json.dumps(response.json(), indent=4))
