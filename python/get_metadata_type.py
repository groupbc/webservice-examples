"""
Example that adds a dummy document to a location then inspects all the
types, metadata fields and available values for the fields (if the type
has a set of available values).  If you take this approach, beware that
metadata in BCDE is specific to a location so don't depend on the values in
one space being the same as those available in another space, even if they
have the same schema.
Compatible with Python 3.9
"""

import json
import urllib.request, urllib.parse, urllib.error
import ws_utils


# Define some utility functions

# The following three functions show examples of constructing the URI
# for endpoints.  The BCDE Web Services API is designed to be
# discoverable which means that you can discover all the endpoints from the
# root endpoint - /api/ - and we will not change the existing keys in our
# published version.
# What may change are the values those keys point to that define our endpoints.
# What the following functions show is constructing three seperate URIs that
# are used in this script.  Were the endpoints to change in a later version,
# these functions would fail, whereas a discoverable approach that looked
# up the values of the endpoints would not.  However, by extrapolating out
# the construction of the URIs to functions, we could modify the code that
# did this if these endpoints were to be superseded and make it discoverable.
def get_type_uri(artifact_uri):
    return artifact_uri + "/type"


def get_contents_uri(artifact_uri):
    return artifact_uri + "/contents"


def get_metadata_uri(artifact_uri):
    return artifact_uri + "/metadata"


# Get the config
CONFIG = ws_utils.get_config()
# Enable logging
LOG_RESPONSE_JSON = CONFIG['development']['logging']['response_json']
LOG_FULL_HTTP_DEBUG = CONFIG['development']['logging']['full_http_debug']
if LOG_FULL_HTTP_DEBUG:
    ws_utils.enable_logging()

# Get the Server URI
SERVER_URI = ws_utils.trim_server_uri(CONFIG['bc']['server_uri'])
# Authenticate and get back the BCDE authenticated OAuth session
bc_session = ws_utils.authenticate(CONFIG)
bc_session.verify = CONFIG['development']['verify_ssl']

# Respect the rate limiter and retry requests that return a 429 error.
ws_utils.rate_limit_requests(bc_session)

CONTAINER_URI = CONFIG['demo']['get_metadata_type']['container_uri']
if not CONTAINER_URI:
    raise RuntimeError("You must set the 'container_uri' parameter to the URI "
                       "of a container that you have access to in config.json "
                       "in order to run this example")

CONTAINER_URI = ws_utils.get_uri(CONTAINER_URI)

# Create a dummy file to perform a lookup on
container_contents_uri = get_contents_uri(CONTAINER_URI)
params={
        'name': "dummy_document",
        'content_type': 'text/plain',
        'content_encoding': 'ascii',
        'content': "",
        'kind': 'document',
    }
dummy_document_returned = bc_session.post(
    container_contents_uri,
    json=params
)
ws_utils.log("POST", container_contents_uri, dummy_document_returned, params)

returned_dummy_document_object = dummy_document_returned.json().get("object", None)
if not returned_dummy_document_object:
    raise RuntimeError("It has not been possible to get the object " 
                       "endpoint from the dummy document that is needed to "
                       "resolve the URI for getting metadata information.")

# Get the URIs for the dummy document's endpoint, adding the EXPAND keyword
# for the keys we wish to have additionally expanded
# out in our response, saving us an additional round trip to the server.
dummy_document_uri = ws_utils.get_uri(returned_dummy_document_object)
query = {
    'EXPAND': 'assignable_types',
}
dummy_document_expanded_uri = \
    ws_utils.get_uri(
        returned_dummy_document_object,
        query,
    )
# Additionally, get the URIs for the type and metadata endpoints for the dummy
# artifact using the utility functions.
dummy_document_artifact_type_uri = get_type_uri(dummy_document_uri)
expand_metadata_on = ['field', ]
dummy_document_artifact_metadata_uri = "%s?%s" % (
    get_metadata_uri(dummy_document_uri),
    urllib.parse.urlencode({'EXPAND': expand_metadata_on}, doseq=1),
)

# Request the expanded endpoint
dummy_document_expanded = bc_session.get(dummy_document_expanded_uri)
ws_utils.log("GET", dummy_document_expanded_uri, dummy_document_expanded)
dummy_document_expanded_json = dummy_document_expanded.json()

# The expanded data comes back in the EXPAND section, keyed by the
# endpoints it comes from.  Lookup the information in the returned JSON
dummy_document_expand_section = dummy_document_expanded_json.get('EXPAND', None)
dummy_document_assignable_types_key = dummy_document_expanded_json.get(
    'assignable_types',
    None
)
if dummy_document_expand_section is None:
    raise RuntimeError("It has not been possible to get the EXPAND "
                       "key from the dummy document endpoint.")
if dummy_document_assignable_types_key is None:
    raise RuntimeError("It has not been possible to get the assignable_types "
                       "key from the dummy document endpoint.")

# Get all assignable types in the artifact
assignable_types = dummy_document_expand_section.get(
    dummy_document_assignable_types_key, {}
)

returned_types = assignable_types.get("types", [])
# Create a dictionary to allow caching of writable_fields by type
writable_fields = {}

# Print out the types.
print("Types")
print(" * %d types found:" % (len(returned_types), ))
for index, type in enumerate(returned_types):
    print("  * %d. %s : %s" % (
        index + 1,
        type.get("name", ""),
        type.get("id", ""),
    ))

# Set the dummy document to each of the types in turn and lookup
# the writeable fields and, if they are listed, the available values.
print("Metadata Fields")
# Get the md fields and possible values
for type in returned_types:
    type_id = type.get("id", "")
    type_name = type.get("name", "")

    # Set the type on the dummy document
    params=type_id
    set_type_result = bc_session.put(
        dummy_document_artifact_type_uri,
        json=params
    )
    ws_utils.log(
        "PUT",
        dummy_document_artifact_type_uri,
        set_type_result,
        {"json": params}
    )

    # Get the metadata on the document
    metadata = bc_session.get(dummy_document_artifact_metadata_uri)
    ws_utils.log("GET", dummy_document_artifact_metadata_uri, metadata)
    metadata_json = metadata.json()

    writable_fields[type_id] = metadata_json.get("writable_fields", [])
    print(" * %s has %d writable fields" % (
        type_name,
        len(writable_fields[type_id]),
    ))

    for index, writable_field in enumerate(writable_fields[type_id]):
        available_values_as_string = ""
        if writable_field.get("available_values") \
                and len(writable_field["available_values"]):
            available_values_as_string = " (values from: [%s])" % (
                ", ".join(writable_field["available_values"]),
            )
        # We've included an expansion on the "field" key in the metadata
        # endpoint so we can find out the "instruction" text provided for
        # each metadata value.
        example_text = ""
        field_expand_key = writable_field["field"]
        if field_expand_key and metadata_json["EXPAND"][field_expand_key]:
            example_text = metadata_json["EXPAND"][field_expand_key].get("instruction", "")
        if example_text == "":
            example_text = "None"

        print("  * %d. %s%s - Example: %s" % (
            index + 1,
            writable_field.get("field_title", ""),
            available_values_as_string,
            example_text,
        ))

# It would probably be a good idea to tidy up and delete the document, but
# I don't show that in this example.
