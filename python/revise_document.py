"""
Example to show how to revise a document in BCDE
Compatible with Python 3.9
"""

import base64
import json
import mimetypes
import os
import ws_utils

# Get the config
CONFIG = ws_utils.get_config()
# Enable logging
LOG_RESPONSE_JSON = CONFIG['development']['logging']['response_json']
LOG_FULL_HTTP_DEBUG = CONFIG['development']['logging']['full_http_debug']
if LOG_FULL_HTTP_DEBUG:
    ws_utils.enable_logging()
# Authenticate and get back the BCDE authenticated OAuth session
bc_session = ws_utils.authenticate(CONFIG)
bc_session.verify = CONFIG['development']['verify_ssl']

# Respect the rate limiter and retry requests that return a 429 error.
ws_utils.rate_limit_requests(bc_session)

# SPACE_ID must be the BCDE ID of the container in which you want the new document
# to live. The BCDE ID can be found through the interface where it will be the
# last path segment of the url to that container. Rather than using a fixed
# container you can also use a search to dynamically find containers (see
# lookup_revise_document.py for an example of a search).
# This is set in the config
SPACE_ID = CONFIG['demo']['revise_document']['space_id']
if not SPACE_ID:
    raise RuntimeError("You must set the 'space_id' parameter to the ID of a "
                       "container you have access to in config.json "
                       "in order to run this example")

# Get the root endpoint to identify the services
root_uri = ws_utils.get_uri('/api/')
response = bc_session.get(
    root_uri
)
# Raise an error if we don't get back a 200
response.raise_for_status()
root = response.json()
ws_utils.log('GET', root_uri, response)

returned_lookup_uri = root.get('services', {}).get('lookup_uri', None)
if returned_lookup_uri is None:
    raise RuntimeError("It has not been possible to get the lookup_uri "
                       "endpoint that is needed to resolve the URI for "
                       "the Space")

artifact_lookup_uri = ws_utils.get_uri(returned_lookup_uri)

# Given an ID of a container in BCDE, lookup its API URI.
params = [SPACE_ID,]
response = bc_session.post(
    artifact_lookup_uri,
    json=params
)
# Raise an error if we don't get back a 200
response.raise_for_status()
artifact_lookup = response.json()
ws_utils.log('POST', artifact_lookup_uri, response, params)
SERVER_URI = CONFIG['bc']['server_uri']
uris = response.json()

if LOG_RESPONSE_JSON:
    print("-----------")
    print("%s/api/services/lookup_uri returned:" % (SERVER_URI, ))
    print(json.dumps(uris, indent=4))
    print("-----------")

# POSTing to the /contents endpoint of a container will create a document in
# it. A new document will always be created. To revise a document you must POST
# to its /versions endpoint instead.
search_uri = ws_utils.get_uri('/api/services/simple_search')

# Look for any item with the name defined in the config value 'previous_name'
params = {
    'location': artifact_lookup[str(SPACE_ID)],
    'klang': "name=%s" % (CONFIG['demo']['revise_document']['previous_name'], ),
    'refresh_if_older': 1,
    'EXPAND': 'uri',
}
response = bc_session.get(
    search_uri,
    params=params,
)
# Raise an error if we don't get back a 200
response.raise_for_status()
found_items = response.json()
ws_utils.log('GET', search_uri, response, params)

# The search we ran would find anything with the given name, not just documents
# so for this we find the first matching item that is a document. It's possible
# that there are several valid documents being returned, it's up to the caller
# to decide the correct one.
document_uri = None
for found_item in found_items.get('contents', []):
    if found_item['kind'] == 'document':
        document_uri = found_item['uri']
        break

if document_uri is not None:
    version_uri = ws_utils.get_uri(
        found_items['EXPAND'][document_uri]['versioning']['versions'],
    )

    new_name = CONFIG['demo']['revise_document']['new_name']

    # Try to get the file binary_file_name_to_upload from disk.
    local_file_name = CONFIG['demo']['revise_document']['local_file_name_for_contents']
    if local_file_name == "" or not os.path.isfile(local_file_name):
        raise RuntimeError("No file found with name '%s'" % local_file_name)

    content_type, content_encoding = mimetypes.guess_type(local_file_name)
    # Ignore the content_encoding as we are going to read in the contents from
    # the file as a binary string and encode it as base64, so we'll use that
    # for the 'content_encoding' below.

    with open(local_file_name, mode='rb') as file_object:
        document_content = file_object.read()

    encoded_document_contents = base64.b64encode(document_content).decode('ascii')

    params = {
        'name': new_name,
        'content_type': content_type,
        'content_encoding': 'base64',
        'content': encoded_document_contents,
    }
    response = bc_session.post(
        version_uri,
        json=params,
    )
    # Raise an error if we don't get back a 200
    response.raise_for_status()
    ws_utils.log('POST', version_uri, response, params)

    # The response will be the URI of the revised document, including it's
    # version label
    print("\n\n%s returned" % (version_uri, ))
    print((json.dumps(response.json(), indent=4)))
else:
    print("\n\nNo document found to revise")
