"""
Example to show how to create a document in BCDE
Compatible with Python 3.9
"""

import base64
import json
import mimetypes
import os
import ws_utils

# Get the config
CONFIG = ws_utils.get_config()
# Enable logging
LOG_RESPONSE_JSON = CONFIG['development']['logging']['response_json']
LOG_FULL_HTTP_DEBUG = CONFIG['development']['logging']['full_http_debug']
if LOG_FULL_HTTP_DEBUG:
    ws_utils.enable_logging()
# Authenticate and get back the BCDE authenticated OAuth session
bc_session = ws_utils.authenticate(CONFIG)
bc_session.verify = CONFIG['development']['verify_ssl']

# Respect the rate limiter and retry requests that return a 429 error.
ws_utils.rate_limit_requests(bc_session)

# SPACE_ID must be the BCDE ID of the container in which you want the new document
# to live. The BCDE ID can be found through the interface where it will be the
# last path segment of the url to that container. Rather than using a fixed
# container you can also use a search to dynamically find containers (see
# lookup_revise_document.py for an example of a search).
# This is set in the config
SPACE_ID = CONFIG['demo']['create_document_from_file']['space_id']
if not SPACE_ID:
    raise RuntimeError("You must set the 'space_id' parameter to the ID of a "
                       "container you have access to in config.json "
                       "in order to run this example")

# Get the root endpoint to identify the services
root_uri = ws_utils.get_uri('/api/')
response = bc_session.get(
    root_uri
)
# Raise an error if we don't get back a 200
response.raise_for_status()
root = response.json()
ws_utils.log('GET', root_uri, response)

returned_lookup_uri = root.get('services', {}).get('lookup_uri', None)
if returned_lookup_uri is None:
    raise RuntimeError("It has not been possible to get the lookup_uri "
                       "endpoint that is needed to resolve the URI for "
                       "the Space")

artifact_lookup_uri = ws_utils.get_uri(returned_lookup_uri)

# Given an ID of a space in BCDE, lookup its API URI.
params = [SPACE_ID,]
response = bc_session.post(
    artifact_lookup_uri,
    json=params
)
# Raise an error if we don't get back a 200
response.raise_for_status()
artifact_lookup = response.json()
ws_utils.log('POST', artifact_lookup_uri, response, params)

space_uri = ws_utils.get_uri(artifact_lookup[str(SPACE_ID)])
response = bc_session.get(
    "%s" % (space_uri, )
)
# Raise an error if we don't get back a 200
response.raise_for_status()
space = response.json()
ws_utils.log('GET', space_uri, response)

# POSTing to the contents endpoint of a container will create a new artifact in
# it. Here we are dealing with documents because in our post to the container's
# contents endpoint we set "kind": "document" (see below).   If we post the
# same thing twice, we will create a new document each time.  To revise a
# document you must POST to the version endpoint for the document, not the
# container. See lookup_revise_document.py for an example of this.
# See the documentation on your BCDE server at the following path for more
# information about the Artifact endpoint:
#    /bc/bc.cgi?op=web_service_documentation&resource=ArtifactResource
space_contents_relative_uri = space.get("contents", None)
if space_contents_relative_uri is None:
    raise RuntimeError("No contents URI was returned for the Space")

space_contents_uri= ws_utils.get_uri(space_contents_relative_uri)

# Try to get the file binary_file_name_to_upload from disk.
file_name = CONFIG['demo']['create_document_from_file']['name']
if file_name == "" or not os.path.isfile(file_name):
    raise RuntimeError("No file found with name '%s'" % file_name)

content_type, content_encoding = mimetypes.guess_type(file_name)
# We don't care what the actual encoding of the file as we're not intending to
# interpret the contents, we just want an exact byte-for-byte copy to be
# uploaded to the server.  To allow binary content in a JSON string it needs to
# be encoded and base64 is suitable.

with open(file_name, mode='rb') as file_object:
    document_content = file_object.read()

encoded_document_contents = base64.b64encode(document_content).decode('ascii')

# POST to the contents of a container to create a document there
params = {
    'name': file_name,
    'content_type': content_type,
    'content_encoding': 'base64',
    'content': encoded_document_contents,
    'kind': 'document',
}
response = bc_session.post(
    space_contents_uri,
    json=params,
)
# Raise an error if we don't get back a 200
response.raise_for_status()
ws_utils.log('GET', space_contents_uri, response, params)

# The response will be the URI of the newly created document
print("\n\n%s returned" % (space_contents_uri, ))
print(json.dumps(response.json(), indent=4, sort_keys=True))
