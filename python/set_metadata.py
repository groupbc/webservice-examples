"""Compatible with Python 3.9"""

import json
import ws_utils

# Get the config
CONFIG = ws_utils.get_config()
# Enable logging
LOG_RESPONSE_JSON = CONFIG['development']['logging']['response_json']
LOG_FULL_HTTP_DEBUG = CONFIG['development']['logging']['full_http_debug']
if LOG_FULL_HTTP_DEBUG:
    ws_utils.enable_logging()
# Authenticate and get back the BCDE authenticated OAuth session
bc_session = ws_utils.authenticate(CONFIG)
bc_session.verify = CONFIG['development']['verify_ssl']

# Respect the rate limiter and retry requests that return a 429 error.
ws_utils.rate_limit_requests(bc_session)

# For this example we will assume we already have the BCDE ID for the artifact
# whose metadata we want to edit
ARTIFACT_ID = CONFIG['demo']['set_metadata']['artifact_id']
if not ARTIFACT_ID:
    raise RuntimeError("You must set the 'artifact_id' parameter to the ID of "
                       "an artifact you have access to in config.json "
                       "in order to run this example")

# Get the root endpoint to identify the services
root_uri =  ws_utils.get_uri("/api/")
response = bc_session.get(
   root_uri
)
# Raise an error if we don't get back a 200
response.raise_for_status()
root = response.json()
ws_utils.log('GET', root_uri, response)

lookup_uri = ws_utils.get_uri(root["services"]["lookup_uri"])

# Given an ID of an artifact in BCDE, lookup its API URI.
params = [ARTIFACT_ID,]
response = bc_session.post(
    lookup_uri,
    json=params,
)
# Raise an error if we don't get back a 200
response.raise_for_status()
uris = response.json()
ws_utils.log('POST', lookup_uri, response, params)

artifact_uri = ws_utils.get_uri(uris[str(ARTIFACT_ID)])
response = bc_session.get(artifact_uri)
# Raise an error if we don't get back a 200
response.raise_for_status()
artifact = response.json()
ws_utils.log('GET', artifact_uri, response)

artifact_metadata_uri = ws_utils.get_uri(artifact["metadata"])

# Get the metadata for the artifact.  This is done by doing a HTTP GET
# to the metadata endpoint.  See the documentation on your BCDE server at the
# following path for more information about this endpoint:
#    /bc/bc.cgi?op=web_service_documentation&resource=MetadataResource
metadata_uri = '%s' % (artifact_metadata_uri, )
response = bc_session.get(
    metadata_uri
)
# Raise an error if we don't get back a 200
response.raise_for_status()
metadata = response.json()
ws_utils.log('GET', metadata_uri, response)

json_mapping = {}
# Get the ids from the writable_field field_title values
# Refer to the set_metadata.md file in the guides folder in these examples for a
# description which keys are required, as these are based on the types
# you are setting, e.g. "text" and "items" keys for Text  and Drop Down fields.
for writable_field in metadata["writable_fields"]:
    if writable_field["field_title"] == \
            CONFIG["demo"]["set_metadata"]["text_field_1"]["field_title"]:
        json_mapping[writable_field["metafield"]] = {
            "text": CONFIG["demo"]["set_metadata"]["text_field_1"]["text"]
        }
    elif writable_field["field_title"] == \
            CONFIG["demo"]["set_metadata"]["text_field_2"]["field_title"]:
        json_mapping[writable_field["metafield"]] = {
            "text": CONFIG["demo"]["set_metadata"]["text_field_2"]["text"]
        }
    elif writable_field["field_title"] == \
            CONFIG["demo"]["set_metadata"]["drop_down_1"]["field_title"]:
        json_mapping[writable_field["metafield"]] = {
            "items": CONFIG["demo"]["set_metadata"]["drop_down_1"]["items"]
        }

# Set the metadata for the artifact.  This is done by doing a HTTP POST
# to the metadata endpoint, passing through the JSON mapping as the body of
# the request.  See the documentation on your BCDE server at the following path
# for more information about this endpoint:
#    /bc/bc.cgi?op=web_service_documentation&resource=MetadataResource
response = bc_session.post(
    metadata_uri,
    json=json_mapping
)
# Raise an error if we don't get back a 200
response.raise_for_status()
response_json = response.json()
ws_utils.log('POST', metadata_uri, response, json_mapping)

print("\n\nSet metadata on %s returned" % (artifact_metadata_uri, ))
print(json.dumps(response_json['values'], indent=4, sort_keys=True))
