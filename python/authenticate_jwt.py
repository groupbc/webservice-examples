"""
Example to show how to authenticate using JWT with BCDE
Compatible with Python 3.9
"""

import json
import requests
import ws_utils

# Get the config, which includes:
# * the Application key
# * the URI of the BCDE server
# * the JWT service user
# * the location of JWT private key
CONFIG = ws_utils.get_config()
# Enable logging
LOG_RESPONSE_JSON = CONFIG['development']['logging']['response_json']
LOG_FULL_HTTP_DEBUG = CONFIG['development']['logging']['full_http_debug']
if LOG_FULL_HTTP_DEBUG:
    ws_utils.enable_logging()

# Set the key, secret and callback values from config
webservice_key = CONFIG['bc']['webservice_key']
server_uri = CONFIG['bc']['server_uri']
# To validate with BCDE the server uri requires a trailing slash when used in
# the aud claim
if not server_uri.endswith('/'):
    server_uri += '/'
service_user = CONFIG['bc']['jwt_service_user']
jwt_private_key = CONFIG['bc']['jwt_private_key_file']

# JWT Authentication step 1: Generate the JWT
import jose.jwt
import time
import uuid

# Private key files should be PEM encoded which is always ASCII.
with open(jwt_private_key, mode='rt', encoding='ascii') as f:
    private_key = f.read()

current_time = time.time()

token = jose.jwt.encode({
    'iss': webservice_key,         # BCDE Webservice Application Key (string)
    'aud': server_uri,             # BCDE server URI
    'sub': 'SERVICE-USER-REQUEST', # Fixed string 'SERVICE-USER-REQUEST'
    'nbf': current_time,           # NumericDate
    'exp': current_time + 30,      # NumericDate
    'jti': str(uuid.uuid4()),      # Random string
    'user': service_user,          # Username (string)
}, private_key, algorithm='ES512')

print("Generated JWT:")
print(token)

# JWT Authentication step 2: Add the authentication to the header and make a call
root_uri = ws_utils.get_uri('/api/')
resp = requests.get(root_uri, headers={
    'Authorization': 'Bearer %s' % token
})

# Raise an error if we don't get back a 200
resp.raise_for_status()
ws_utils.log('GET', root_uri, resp)
root = resp.json()
print("\n\nGET of %s returned" % (root_uri, ))
print(json.dumps(root, indent=4, sort_keys=True))
