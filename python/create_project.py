"""
Example to show how to create a project in BCDE
Compatible with Python 3.9
"""

import json
import ws_utils

# Get the config
CONFIG = ws_utils.get_config()
# Enable logging
LOG_RESPONSE_JSON = CONFIG['development']['logging']['response_json']
LOG_FULL_HTTP_DEBUG = CONFIG['development']['logging']['full_http_debug']
if LOG_FULL_HTTP_DEBUG:
    ws_utils.enable_logging()
# Authenticate and get back the BCDE authenticated OAuth session
bc_session = ws_utils.authenticate(CONFIG)
bc_session.verify = CONFIG['development']['verify_ssl']

# Respect the rate limiter and retry requests that return a 429 error.
ws_utils.rate_limit_requests(bc_session)

# Use the authenticated BCDE OAuth session to make requests
root_uri = ws_utils.get_uri('/api/')
response = bc_session.get(
    root_uri
)
# Raise an error if we don't get back a 200
response.raise_for_status()
root = response.json()
ws_utils.log('GET', root_uri, response)

home_uri = ws_utils.get_uri(root['home'])
response = bc_session.get(
    home_uri
)
# Raise an error if we don't get back a 200
response.raise_for_status()
home = response.json()
ws_utils.log('GET', home_uri, response)

# POST to the contents of a location to create a project there
create_project_uri = ws_utils.get_uri(home['contents'])
params = {
    'name': CONFIG['demo']['create_project']['name'],
    'descr': CONFIG['demo']['create_project']['description'],
    'kind': "project"
}
response = bc_session.post(
    create_project_uri,
    json=params
)
# Raise an error if we don't get back a 200
response.raise_for_status()
create_project = response.json()
ws_utils.log('GET', create_project_uri, response)

print("\n\nCreated project %s" % create_project['artifact'])
