"""
Example to show running a simple search in BCDE
Compatible with Python 3.9
"""

import json
import ws_utils

# Get the config
CONFIG = ws_utils.get_config()
# Enable logging
LOG_FULL_HTTP_DEBUG = CONFIG['development']['logging']['full_http_debug']
if LOG_FULL_HTTP_DEBUG:
    ws_utils.enable_logging()
# Authenticate and get back the BCDE authenticated OAuth session
bc_session = ws_utils.authenticate(CONFIG)
bc_session.verify = CONFIG['development']['verify_ssl']

# Respect the rate limiter and retry requests that return a 429 error.
ws_utils.rate_limit_requests(bc_session)

# Get the KLANG query from config.
KLANG_SEARCH_QUERY = CONFIG['demo']['search']['query']
if not KLANG_SEARCH_QUERY:
    raise RuntimeError("You must set the 'query' parameter to the "
                       "KLANG search you want to run in config.json "
                       "in order to run this example")

# Get the root endpoint to identify the services
root_uri = ws_utils.get_uri("/api/")
response = bc_session.get(root_uri)
# Raise an error if we don't get back a 200
response.raise_for_status()
root = response.json()
ws_utils.log('GET', root_uri, response)

returned_simple_search_uri = root.get('services', {}).get('simple_search', None)
if returned_simple_search_uri is None:
    raise RuntimeError("It has not been possible to get the simple_search "
                       "endpoint that is needed to resolve the URI for "
                       "the search.")

search_results_uri = ws_utils.get_uri(returned_simple_search_uri)

# Here we make a simple klang query that's defined in config.  The default
# search looks for everything where the value of the metadata field with id
# 1202 is "Two" AND the artifiact is either a space or a folder.
# See section 6.7 of the BCDE help for more info on klang queries.
params = {
    'klang': KLANG_SEARCH_QUERY,
}
response = bc_session.get(
    search_results_uri,
    params=params,
)
# Raise an error if we don't get back a 200
response.raise_for_status()
search_results = response.json()
ws_utils.log('GET', search_results_uri, response, params)

# The information we get back is paginated, and is essentially a listing
# in BCDE. Just like a listing, we need to check to see if the total
# is an exact count or an approximation (because further pages may contain
# results which would be filtered out by access rights).
is_exact_string = ''
if not search_results.get("is_total_exact"):
    is_exact_string = ' about'
# Check to see if there are more pages of results available
more_pages_available_string = ''
if search_results.get("next"):
    more_pages_available_string = '  More pages available.'

print("\n\nSearch on %s" % (search_results_uri, ))
# Show the number of results
print("Found%s %d result(s).%s" % (
    is_exact_string,
    search_results.get("total", 0),
    more_pages_available_string,
))
# List out the results showing in this page of
print("Showing %d result(s):" % (len(search_results.get("contents", [])), ))
for index, content in enumerate(search_results.get("contents", [])):
    print(" * %d. %s : %s" % (
        index + 1,
        content.get("name", ""),
        content.get("uri", "")
    ))
