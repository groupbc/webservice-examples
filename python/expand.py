"""
Example to show running a saved search in BCDE using the EXPAND keyword.
This allows us to expand out the returned values for a given key so we
don't need to make additional API calls.  This can be useful when we run
a search and want to return information such as the file size for each of those
documents, something that is not returned in the search results.
Compatible with Python 3.9
"""

import json
import ws_utils

# Get the config
CONFIG = ws_utils.get_config()
# Enable logging
LOG_FULL_HTTP_DEBUG = CONFIG['development']['logging']['full_http_debug']
if LOG_FULL_HTTP_DEBUG:
    ws_utils.enable_logging()
# Authenticate and get back the BCDE authenticated OAuth session
bc_session = ws_utils.authenticate(CONFIG)
bc_session.verify = CONFIG['development']['verify_ssl']

# Respect the rate limiter and retry requests that return a 429 error.
ws_utils.rate_limit_requests(bc_session)

# Get the KLANG query from the config.  We assume that this is set so we do
# not do any error checking.
KLANG_SEARCH_QUERY = CONFIG['demo']['expand']['query']
# Get the key we are going to expand on from config
EXPAND_ON_KEY = CONFIG['demo']['expand']['expand_key']
if not EXPAND_ON_KEY:
    raise RuntimeError("You must set the 'expand_key' parameter to the "
                       "key you want to expand in config.json "
                       "in order to run this example")
DISPLAY_KEY = CONFIG['demo']['expand']['display_key']
DISPLAY_KEY_SUFFIX = CONFIG['demo']['expand']['display_key_suffix']

# Get the root endpoint to identify the services
root_uri = ws_utils.get_uri("/api/")
response = bc_session.get(root_uri)
# Raise an error if we don't get back a 200
response.raise_for_status()
root = response.json()
ws_utils.log('GET', root_uri, response)

returned_simple_search_uri = root.get('services', {}).get('simple_search', None)
if returned_simple_search_uri is None:
    raise RuntimeError("It has not been possible to get the simple_search "
                       "endpoint that is needed to resolve the URI for "
                       "the search.")

search_results_uri = ws_utils.get_uri(returned_simple_search_uri)

# Here we make a simple klang query that's defined in config.  The default
# search looks for everything where the artifiact is a document.
# See section 6.7 of the BCDE help for more info on klang queries
params = {
    'klang': KLANG_SEARCH_QUERY,
    'EXPAND': EXPAND_ON_KEY,
}
response = bc_session.get(
    search_results_uri,
    params=params,
)
# Raise an error if we don't get back a 200
response.raise_for_status()
search_results = response.json()
ws_utils.log('GET', search_results_uri, response, params)

# Check to see if the total is an exact count or an approximation
is_exact_string = ''
if not search_results.get("is_total_exact"):
    is_exact_string = ' about'
# Check to see if there are more pages of results available
more_pages_available_string = ''
if search_results.get("next"):
    more_pages_available_string = '  More pages available.'

# Get the expanded dict
expanded_dict = search_results.get("EXPAND", {})

print("\n\nSearch on %s" % (search_results_uri, ))
# Show the number of results
print("Found%s %d result(s).%s" % (
    is_exact_string,
    search_results.get("total", 0),
    more_pages_available_string,
))
# List out some information for the first page of results returned, including
# a key in the expanded results along with a suffix.  The default is to show
# the document_size followed by the string "bytes".
print("Showing %d result(s):" % (len(search_results.get("contents", [])), ))
for index, content in enumerate(search_results.get("contents", [])):
    expanded_field = None

    # Expanded fields are returned in a top level property called EXPAND.
    # Each property that we expand on will have the value of the key we
    # expanded on as the key in the EXPAND property.
    # E.g., if we expand on the key 'uri' in search results, and we had
    # two artifacts returned, we use the value of the 'uri', so we would use
    # the key ```search_results['contents'][0]['uri']``` in EXPAND.
    if content.get(EXPAND_ON_KEY, "") in expanded_dict:
        expanded_field = expanded_dict[content.get(EXPAND_ON_KEY)].get(
            DISPLAY_KEY, ""
        )
    print(" * %d. %s : %s : %s %s" % (
        index + 1,
        content.get("name", ""),
        content.get("uri", ""),
        expanded_field,
        DISPLAY_KEY_SUFFIX,
    ))
