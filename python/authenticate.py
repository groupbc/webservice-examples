"""
Example to show how to authenticate using OAuth with BCDE
Compatible with Python 3.9
"""

import json
import requests
import requests_oauthlib
import webbrowser
import ws_utils

# Get the config, which includes:
# * the OAuth key and secret
# * the OAuth callback
# * the URI of the BCDE server
# * the "verify"
CONFIG = ws_utils.get_config()
# Enable logging
LOG_RESPONSE_JSON = CONFIG['development']['logging']['response_json']
LOG_FULL_HTTP_DEBUG = CONFIG['development']['logging']['full_http_debug']
if LOG_FULL_HTTP_DEBUG:
    ws_utils.enable_logging()

# Set the key, secret and callback values from config
webservice_key = CONFIG['bc']['webservice_key']
webservice_secret = CONFIG['bc']['webservice_secret']
oauth_callback_uri = CONFIG['bc']['oauth_callback_uri']

# We're using the requests_oauthlib to assist in the OAuth workflow,
# so we create a new OAuth1Session with the key and secret in order to
# then be able to go through the OAuth workflow with the convenience methods
# on the OAuth1Session object.
bc_session = requests_oauthlib.OAuth1Session(
    webservice_key,
    client_secret=webservice_secret,
    callback_uri=oauth_callback_uri,
)

# OAuth step 1: Fetch the request token
request_uri = ws_utils.get_uri('/oauth/request_token')
try:
    bc_session.fetch_request_token(
        request_uri,
        verify=CONFIG['development']['verify_ssl'],
    )
except requests.exceptions.SSLError as e:
    raise RuntimeError("The requests.exceptions.SSLError has been raised. "
                       "If you are running this demo code on a "
                       "development server, you might want to set the "
                       "config setting 'verify_ssl' to false.  You should "
                       "NOT do this in production. "
                       "Full error follows:\n%s" % str(e))

# OAuth step 2: Generate the authorisation URL, which will include the
# OAuth token, and present the URL to the user so they can authorise the
# application.  By following the link, the user will be presented with the
# OAuth authorisation form in BCDE.
auth_url = bc_session.authorization_url(
     ws_utils.get_uri('/oauth/authorise'),
)

# In a real application, rather than a command line example, this is the
# point where your app would redirect them to the OAuth login, get them
# to authorise this application to do things on their behalf, and, by
# providing the oauth_callback, redirect them back to your application,
# which will include the verifier parameter

# Here we simply get their browser to open up the URL.
print("Attempting to open %s in your browser.  Copy-and-paste this URL " \
      "into your browser if this doesn't happen automatically." \
      % (auth_url,))
webbrowser.open(auth_url)

# Get the user to paste in the redirect URL which will contain the OAuth
# Verifier after they have logged in.

# We can enable a verifier by registering the application in BCDE as using a
# verifier.  If we do, then that will be displayed on the authorise screen
# after the user has authorised as well as being included in the auth
# redirection url.  However, this is only fully implemented from
# BCDE 7.2.2 onwards and only if the use_verifier_token=yes has been
# supplied when registering the application in BCDE.
redirect_response = input(
    "Paste the full redirect URL here once you have logged in " +
    "and press enter..."
)

# We need to provide a non-empty string for verifier, so we try to get
# it from the response URL.
authorisation_response_parsed = bc_session.parse_authorization_response(
    redirect_response
)

# Check to make sure the verifier is passed through.  Pre BCDE 7.2.2, this was
# verifier value was not checked, so I am setting the default value here to
# u"admin" which will be ignored.  If you do not provide a value for the
# verifier, requests_oauthlib will raise the following error:
# - requests_oauthlib.oauth1_session.VerifierMissing
verifier = \
    authorisation_response_parsed.get(
        "oauth_verifier",
        "dummy_for_library_compatibility"
    )

# OAuth step 3: Fetch the authenticated access token.  This is stored
# in bc_session.token and can be saved and used in subsequent requests for
# as long as it remains valid.
bc_session.fetch_access_token(
     ws_utils.get_uri('/oauth/access_token'),
    verifier=verifier
)

# Use the authenticated BCDE OAuth session to make requests.
# Here we simply call the root of the API and print out the JSON that
# is returned.
root_uri = ws_utils.get_uri('/api/')
response = bc_session.get(
    root_uri,
)
# Raise an error if we don't get back a 200
response.raise_for_status()
root = response.json()
print("\n\nGET of %s returned" % (root_uri, ))
print(json.dumps(root, indent=4, sort_keys=True))
