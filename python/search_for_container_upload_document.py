"""
Example of searching for a container in BCDE and uploading a document to it
Compatible with Python 3.9
"""

import json
import ws_utils

# Get the config
CONFIG = ws_utils.get_config()
# Enable logging
LOG_RESPONSE_JSON = CONFIG['development']['logging']['response_json']
LOG_FULL_HTTP_DEBUG = CONFIG['development']['logging']['full_http_debug']
if LOG_FULL_HTTP_DEBUG:
    ws_utils.enable_logging()
# Authenticate and get back the BCDE authenticated OAuth session
bc_session = ws_utils.authenticate(CONFIG)
bc_session.verify = CONFIG['development']['verify_ssl']

SPACE_NAME = CONFIG['demo']['search_for_container_upload_document']['space_name']
if not SPACE_NAME:
    raise RuntimeError("You must set the 'space_name' parameter to the name "
                       "of a space you have access to in config.json "
                       "in order to run this example")

# Get the user's home which is stored in the /api/ endpoint
root_uri = ws_utils.get_uri('/api/')
response = bc_session.get(
    root_uri
)
# Raise an error if we don't get back a 200
response.raise_for_status()
root = response.json()
ws_utils.log('GET', root_uri, response)

if root.get('services', '') != '' \
        and root.get('services').get('simple_search'):
    search_uri = ws_utils.get_uri(root.get('services').get('simple_search'))

else:
    raise RuntimeError('The root endpoint did not return a simple_search ')

# Construct a search using BCDE's Keyword language.  See section 6.7 of the
# help for information on how to use this.
# Here we look for a Space with a defined name in the user's home
params = {
     'location': root.get('home', ''),
     'klang': "(name:%s) and (class:%s)" % (
         SPACE_NAME,
         'space'
     ),
     'refresh_if_older': 1,
}
response = bc_session.get(
    search_uri,
    params=params,
)
# Raise an error if we don't get back a 200
response.raise_for_status()
found_items = response.json()
ws_utils.log('GET', search_uri, response, params)

# Get the first matching item, if ones exists, and upload a file to it.
if len(found_items.get('contents', [])) < 1:
    print("\n\nNo spaces found with the name '%s'." % (SPACE_NAME,))
else:
    # Get the contents index of the items returned, look at the first
    # item, then get its uri endpoint
    first_space_uri = ws_utils.get_uri(found_items['contents'][0].get('uri'))
    response = bc_session.get(
        first_space_uri
    )
    # Raise an error if we don't get back a 200
    response.raise_for_status()
    first_space = response.json()
    ws_utils.log("GET", first_space_uri, response)

    if first_space.get('contents', None) is not None:
        contents_uri = ws_utils.get_uri(first_space.get('contents'))

        # Create a text file that contains the string in config 'content' by
        # POSTing to the contents of a container to create a document there
        params = {
            # The file name to be created
            'name': CONFIG['demo']['search_for_container_upload_document']['document_name'],
            # The content type of the file we're creating.
            'content_type': 'text/plain',
            # The encoding of the file contents.
            'content_encoding': 'ascii',
            # The content to be posted.  The encoding must match the
            # 'content_encoding" above.
            'content': CONFIG['demo']['search_for_container_upload_document']['content'],
            # The "type" of artifact we're creating.  When creating a document
            # this needs to be set to the string 'document'.
            'kind': 'document',
        }
        response = bc_session.post(
            contents_uri,
            json=params,
        )
        # Raise an error if we don't get back a 200
        response.raise_for_status()
        response_json = response.json()
        # For more information about the "kind", go to the following URL on a
        # BCDE server
        #    /bc/bc.cgi?op=web_service_documentation&resource=ContentsResource

        # The response will be the URI of the newly created document
        ws_utils.log('POST', contents_uri, response, params)

        returned_object_uri = response_json.get('object', None)
        if returned_object_uri is None:
            raise RuntimeError("An object was not returned.")

        # Get the details of the newly created document
        document_uri = ws_utils.get_uri(returned_object_uri)
        response = bc_session.get(
            document_uri
        )
        # Raise an error if we don't get back a 200
        response.raise_for_status()
        ws_utils.log('GET', document_uri, response)

        print("\n\nCreated document %s" % (returned_object_uri, ))
        print((json.dumps(response.json(), indent=4, sort_keys=True)))
