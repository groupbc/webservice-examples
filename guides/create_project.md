# Creating a Space

This guide describes the functionality in the example script
[create_project.py](../python/create_project.py).

## TOC

 * [Before you start](#before-you-start)
    * [Rate Limiting](#rate-limiting)
 * [Accessing the Web Services root](#accessing-the-web-services-root)
 * [Finding the User's Home](#finding-the-users-home)
 * [Creating the new Space](#creating-the-new-space)
 * [Sample output from the script](#sample-output-from-the-script)

## <a name="before-you-start"></a> Before you start

If you are running the python example script while following along, make sure
to read the Python Examples [README.md](../python/README.md) file to ensure you
have the required libraries installed and the correct config set.

Additionally, this example does not deal with authentication, so you should
refer to [authenticate.md](authenticate.md) if you have any problems
authenticating.  We will use the `ws_utils.authenticate()` function as a
starting point for any request as discussed in the
[Using the Access Token to make a request to BCDE](../guides/authenticate.md#using-the-access-token-to-make-a-request-to-bcde)
section.

### <a name="rate-limiting"></a> Rate Limiting

Requests made to the BCDE API may be rate limited. All responses from the BCDE
API should be checked for a status code of `429`, on receiving such a status
code the request should be retried but not before the number of seconds
specified in the `Retry-After` header have passed.

## <a href="accessing-the-web-services-root"></a> Accessing the Web Services root

The root URI for the BCDE Web Services is exposed as `/api/`.  As mentioned in
[REST and Discoverability](../python/README.md#rest-and-discoverability), from
the root, we are able to discover all the endpoints available.  Once
authenticated you can make a `GET` request to this endpoint, and you will get a
JSON response like this:

```json
{
    "artifacts": "/api/artifacts",
    "current_state": {
        "user": "/api/artifacts/user-492",
        "time": "2019-07-03T09:31:21.689726+00:00"
    },
    "metadata_datatypes": "/api/metadata_datatypes",
    "bag": "/api/artifacts/bag-508",
    "services": {
        "simple_search": "/api/services/simple_search?",
        "lookup_id": "/api/services/lookup_id",
        "lookup_user_addressbook": "/api/services/lookup_user_addressbook",
        "lookup_uri": "/api/services/lookup_uri"
    },
    "home": "/api/artifacts/home-497",
    "server_logo": "/api/server_logo",
    "waste": "/api/artifacts/waste-503",
    "schema_bag": "/api/artifacts/metadatabag-563",
    "experimental": {
        "templates": "/api/templates/experimental"
    }
}
```

In our example, we do this as follows:
```python
root = bc_session.get(
    "https://centos7/api/"
)
```

Note: This example refers to my local server with the URL `https://centos7/`.

## <a name="finding-the-users-home"></a> Finding the User's Home

For our example, we want to create a new Space in the user's home,
so we find the URI for the `home` in the root endpoint and make a `GET` request
for it.

In the above example, the `home` URI is `/api/artifacts/home-497`, and
requesting that will give us a JSON representation of the user's home that
looks something like this:

```json
{
    "members": "/api/artifacts/home-497/members",
    "web_ui_link_lp": {
        "url": "https://centos7/bc/bc.cgi/0/497?op=lp&id=497",
        "requires_entry": false,
        "can_perform": true,
        "name": "lp",
        "title": "lp"
    },
    "contents": "/api/artifacts/home-497/contents",
    "descr": "",
    "access_rights": "/api/artifacts/home-497/access_rights",
    "web_ui_links": "/api/artifacts/home-497/web_ui_links",
    "web_ui_link_get": {
        "url": "https://centos7/bc/bc.cgi/0/497",
        "requires_entry": false,
        "can_perform": true,
        "name": "get",
        "title": "Get"
    },
    "containers": [],
    "metadata": "/api/artifacts/home-497/metadata",
    "kind": "home",
    "last_modified": "2019-07-03T14:19:23.164171+00:00",
    "view_preference": "/api/artifacts/home-497/view_preference",
    "services": {
        "lookup_user": "/api/artifacts/home-497/services/lookup_user",
        "navigation": "/api/artifacts/home-497/services/navigation"
    },
    "path": [],
    "icon": {
        "default": {
            "width": 52,
            "href": "https://centos7/bscw_icons/home.gif",
            "alt_text": "Home",
            "height": 52
        }
    },
    "owners": "/api/artifacts/home-497/owners",
    "name": "Home",
    "created": {
        "login_name": "admin",
        "user": "/api/artifacts/user-492",
        "full_name": "D'Administrator",
        "time": "2019-03-08T13:25:07.240791+00:00"
    },
    "modified": {
        "login_name": "admin",
        "user": "/api/artifacts/user-492",
        "full_name": "D'Administrator",
        "time": "2019-03-08T13:25:07.240791+00:00"
    },
    "project": null,
    "new_object_types": {
        "project": "/api/artifacts/home-497/types_for/project",
        "folder": "/api/artifacts/home-497/types_for/folder",
        "document": "/api/artifacts/home-497/types_for/document"
    },
    "issue_location": "/api/artifacts/home-497/issues"
}
```

In our example, we `GET` the endpoint as follows:

```python
home_uri = "%s%s" % (SERVER_URI, root['home'], )
home = bc_session.get(
    home_uri
)
```

## <a name="creating-the-new-space"></a> Creating the new Space

Spaces can be created in a user's home and in some other containers, such as
another Space, if the user has permission to do so.  This will depend on the
server settings and also their access rights in the container in which they
are trying to create the Space.  For more information on Spaces and Space
management, see section 5.0 of the online help, found at
`https://[server_address]/bchelp/sec-5-0.html` where `[server_address]` is the
name of the BCDE server.

To create the Space we need to make a `POST` request to the contents
of the home location, providing a JSON object with the following keys:
* `name` - The name of the new Space.
* `descr` - The description of the new Space.
* `kind` - The type of the artifact we're creating.  We use 'project' to create a Space.

The JSON object should look something like the following:

```json
{
    "name": "New Space",
    "descr": "Created using a webservice",
    "kind": "project"
}
```

When we make this `POST` request, the BCDE server will respond with the URI of the
artifact it has created like this:

```json
{
    "artifact": "/api/artifacts/project-14419"
}
```

This URI can be recorded for later use or used immediately to access the
artifact and make further changes to it.

In our example, we `POST` to the home contents endpoint as follows:

```python
create_space_uri = "%s%s" % (SERVER_URI, home['contents'])
params = {
    'name': "New Space",
    'descr': "Created using a webservice",
    'kind': "project"
}
create_space = bc_session.post(
    create_space_uri,
    json=params
)
```

## <a name="sample-output-from-the-script"></a> Sample output from the script

* See [create_project_debug.log](../python/data/create_project_debug.log) for an
example of the full HTTP headers that are sent.
* See [create_project_payload.txt](../python/data/create_project_payload.txt) for
an example of the JSON returned.
