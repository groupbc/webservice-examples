# Authentication

This guide is for the example script [authenticate.py](../python/authenticate.py).

BCDE uses version 1.0a of OAuth. There are OAuth libraries for
most languages, and we recommend using one of these rather than using raw HTTP
requests.

A complete description of the protocol is beyond the scope of this guide. For
further information, see:

 * [OAuth Core 1.0a](http://oauth.net/core/1.0a/)
 * [RFC 5849: The OAuth 1.0 Protocol](http://tools.ietf.org/html/rfc5849)

## TOC

 * [Before you start](#before-you-start)
 * [The Authorisation Flow](#the-authorisation-flow)
    * [Get a Request Token](#get-a-request-token)
    * [User authorises the Request Token](#user-authorises-the-request-token)
    * [Exchange the Request Token for an Access Token](#exchange-the-request-token-for-an-access-token)
 * [Using the Access Token to make a request to BCDE](#using-the-access-token-to-make-a-request-to-bcde)
 * [Sample output from the script](#sample-output-from-the-script)
 * [Notes](#notes)

## <a name="before-you-start"></a> Before you start

Before you can authenticate using OAuth with a BCDE server, you will need to have:
 * Your application registered on that BCDE server
 * Once your application is registered, BCDE Support will provide you with a key
 and secret pair that you will need in this example
 * You will also need the BCDE Server URI

To request that your application is registered on your BCDE server, you will
need to get the approval of your BCDE Server Owner.  If you do not know who this
is, contact [bc.support@bentley.com](mailto:bc.support@bentley.com) and they will
help you.

You will require the login details for a valid BCDE user on the server you are
connecting to.  These are required during the Token Authorisation stage when
you will be prompted to enter the username and password.

If you are running the Python example script while following along, make sure
to read the [README.md](../python/README.md) file in the python folder to
ensure you have the required libraries installed and the correct config set.

## <a name="the-authorisation-flow"></a> The Authorisation Flow

There are three steps required for getting an authenticated Access Token,
also known as the Three-Legged Flow:
 * The application gets an unauthorised Request Token
 * The user authorises the Request Token
 * The application exchanges the Request Token for an Access Token

For more information on this, refer to:
 * [Authenticating with OAuth](https://oauth.net/core/1.0a/#anchor9) from the [OAuth specification](http://oauth.net/core/1.0a/), and
 * Twitter's Developer documentation on [3-legged OAuth authentication](https://developer.twitter.com/en/docs/basics/authentication/overview/3-legged-oauth).

### <a name="get-a-request-token"></a> Get a Request Token

The first step of OAuth authentication is for the application to get a Request
Token.

This is done by making a `POST` request to the Request Token endpoint.  The
header must contain an `Authorization` header with the auth-scheme name set to
the case-sensitive value of `OAuth` and containing the following parameters:
* `oauth_nonce`
* `oauth_timestamp`
* `oauth_version`
* `oauth_signature_method`
* `oauth_consumer_key`
* `oauth_callback`
* `oauth_signature`

The value of `oauth_consumer_key` is the key that BCDE Support have provided to you
and the `oauth_signature` is generated using the secret that was provided to
you.  To see how a request is signed, you can refer to
[section 9 Signing Requests](https://oauth.net/core/1.0a/#signing_process) of
[RFC 5849: The OAuth 1.0 Protocol](http://tools.ietf.org/html/rfc5849).

The `oauth_callback_uri` is the URI that BCDE will redirect the browser to once
the user has authorised the Request Token.

An example of the `Authorization` header that makes up the `POST` request is
as follows:
```http request
Authorization:
	OAuth
		oauth_nonce="124254276207965385981561373775",
		oauth_timestamp="1561373775",
		oauth_version="1.0",
		oauth_signature_method="HMAC-SHA1",
		oauth_consumer_key="ecb03487-b4fe-4e0b-9766-f40fbe01292b",
		oauth_callback="http%3A%2F%2F127.0.0.1%3A5000%2Foauth_app%2Fauthorised",
		oauth_signature="kvP1LcgUxuIfvgTGRinSTpUA2q0%3D"
```

The server responds with the temporary Request Token that contains the token
identifier, `oauth_token`, and the token shared-secret, `oauth_token_secret`.

An example of what this looks like is:

```json
{
    "oauth_token_secret": "23eb-d497-79fd-874b-7f00-7787-7f80-a2e2",
    "oauth_token": "0e3f9450-16f2-404d-8614-4a533ba2afb2"
}
```

See the [authenticate_debug.log](../python/data/authenticate_debug.log) to see
this example header in full.

For more information on the Authorization Header, see [section 3.5.1](https://tools.ietf.org/html/rfc5849#section-3.5.1)
of [RFC 5849: The OAuth 1.0 Protocol](http://tools.ietf.org/html/rfc5849).

In our example code using `requests_oauthlib`, we start this process by
creating a new `OAuth1Session` with the key and secret.

We can make the following call to get a new
session:

```python
bc_session = requests_oauthlib.OAuth1Session(
    "0fc7e5c6-a338-45d3-a8cc-6adc7ff8ffd1",
    client_secret="d62c-3041-e196-ea68-0f65-7a0c-806c-6626",
    callback_uri="http://127.0.0.1:5000/oauth_app/authorised",
)
```

We also pass through an OAuth Callback URI which is the URI for our application
that receives the authenticated Access Token.

Once we have this session, we can fetch the Request Token by calling the
`fetch_request_token` convenience method.

```python
bc_session.fetch_request_token(
    "%s/oauth/request_token" % SERVER_URI
)
```

We also pass through the Boolean `verify` parameter, which is set to `True` in
the default config, to specify that we wish to verify the SSL certificate of the
URL.  In the library this defaults to the value `True`, but we add it in to
make it easier to switch this off for development environments where the
certificate is self-signed or not actually signed.

Additionally, use a `try` statement when calling `fetch_request_token`, making
the first step:

```python
bc_session = requests_oauthlib.OAuth1Session(
    "0fc7e5c6-a338-45d3-a8cc-6adc7ff8ffd1",
    client_secret="d62c-3041-e196-ea68-0f65-7a0c-806c-6626",
    callback_uri="http://127.0.0.1:5000/oauth_app/authorised",
)

try:
    bc_session.fetch_request_token(
        "%s/oauth/request_token" % SERVER_URI,
        verify=CONFIG['development']['verify_ssl'],
    )
except requests.exceptions.SSLError as e:
    raise RuntimeError("An error of type requests.exceptions.SSLError has been raised. "
                       "If you are running this demo code on a "
                       "development server, you might want to set the "
                       "config setting 'verify_ssl' to false.  You should "
                       "NOT do this in production. "
                       "Full error follows:\n%s" % str(e))
```

### <a name="user-authorises-the-request-token"></a> User authorises the Request Token

The user now needs to authorise the application to act on their behalf.  To
facilitate this we need to generate the authorisation URL, which will include the
Request Token we fetched in the first step, and present the URL to the user so
they can authorise the application with their BCDE username and password.  By
following the link, the user will be presented with the OAuth authorisation
form in BCDE.

We simply construct the URL that passes the Request Token `oauth_token` to the
Authorisation endpoint.

An example of this URL, which we assign to `auth_url` in our script, is as
follows:
```
https://centos7/oauth/authorise?oauth_token=0e3f9450-16f2-404d-8614-4a533ba2afb2
```

BCDE will respond by redirecting the user to the OAuth Callback URI that was
included in the first step.

In the example script, we use the `authorization_url` convenience method to
generate the URL, then we open this URL in the web browser:

```python
auth_url = bc_session.authorization_url(
    "%s/oauth/authorise" % SERVER_URI,
)

print "Attempting to open %s in your browser.  Copy-and-paste this URL " \
      "into your browser if this doesn't happen automatically." \
      % (auth_url,)
webbrowser.open(auth_url)
```

### <a name="exchange-the-request-token-for-an-access-token"></a> Exchange the Request Token for an Access Token

When the BCDE server redirects the user back to your website after they have
signed in, the redirect will include `oauth_token` and `oauth_verifier`
parameters.

An example redirect is the following:

```
http://127.0.0.1:5000/oauth_app/authorised?oauth_verifier=admin&oauth_token=0e3f9450-16f2-404d-8614-4a533ba2afb2
```

The Request Token is exchanged for an Access Token by making a `POST` request
that includes an `Authorization` header with the following `OAuth` parameters:

* `oauth_nonce`
* `oauth_timestamp`
* `oauth_version`
* `oauth_signature_method`
* `oauth_consumer_key`
* `oauth_token`
* `oauth_verifier`
* `oauth_signature`

The `oauth_token` is the value we received back in the previous 2 stages.  The
`oauth_verifier` is the value we got back from the BCDE server which we should
validate against the user our script thinks it should be authenticated as.  We
also sign the request using the secret.

An example of the `Authorization` header that makes up the `POST` request is
as follows:

```http request
Authorization:
	OAuth
		oauth_nonce="8819943877433904591561373783",
		oauth_timestamp="1561373783",
		oauth_version="1.0",
		oauth_signature_method="HMAC-SHA1",
		oauth_consumer_key="ecb03487-b4fe-4e0b-9766-f40fbe01292b",
		oauth_token="0e3f9450-16f2-404d-8614-4a533ba2afb2",
		oauth_verifier="admin",
		oauth_signature="ax5GIqY9qCZyWxm4SdaY36%2FlvrQ%3D"
```

This responds with the authorised Access Token that contains the token identifier,
`oauth_token`, and the token shared-secret, `oauth_token_secret`.

An example of what this looks like is:

```json
{
    "oauth_token_secret": "23eb-d497-79fd-874b-7f00-7787-7f80-a2e2",
    "oauth_token": "6f3dfad3-3853-4c51-971f-07cffacc2c67"
}
```

This Access Token is what we need to include whenever we interact with the
BCDE web services.

In a real application, the OAuth Callback URI would probably be a URL (in a web
application), a custom URL scheme (in an iOS app) or an Android App Link.
This is the URI that the application tells BCDE they want BCDE to call when a user
is authorised.  This request will contain authorisation information that BCDE has
generated, such as the token and token secret information for the authorised
Request Token along with verifier information for the user who authenticated
the token.

So, how do we carry this out in our demo script?

Once we have opened the URL in the web browser, we wait for the user to
authenticate with BCDE, then copy the authorisation response URL and paste it
to the command line:

```python
redirect_response = raw_input(
    "Paste the full redirect URL here once you have logged in " +
    "and press enter..."
)
```

We then parse the authorisation information using the
`parse_authorization_response` convenience method:

```python
authorisation_response_parsed = bc_session.parse_authorization_response(
    redirect_response
)
```

We then get the verifier information that BCDE has returned.  Prior to BCDE 7.2.2,
this verifier value was not required.  If you do not provide a value for the
verifier, requests_oauthlib will raise the following error:
```python
requests_oauthlib.oauth1_session.VerifierMissing
```

We retrieve the verifier information from the parsed response's
`oauth_verifier` key
```python
verifier = authorisation_response_parsed["oauth_verifier"]
```

Once we have the verifier, we pass it through to the `fetch_access_token`
convenience method which updates our session with the authorised
Access Token.

This Access Token can be saved and used in subsequent requests for as long as
it remains valid:

```python
bc_session.fetch_access_token(
    "%s/oauth/access_token" % SERVER_URI,
    verifier=verifier
)
```

Altogether, the example for this final step is:
```python
redirect_response = raw_input(
    "Paste the full redirect URL here once you have logged in " +
    "and press enter..."
)

authorisation_response_parsed = bc_session.parse_authorization_response(
    redirect_response
)

verifier = \
    authorisation_response_parsed.get(
        "oauth_verifier",
        u"dummy_for_library_compatibility"
    )

bc_session.fetch_access_token(
    "%s/oauth/access_token" % SERVER_URI,
    verifier=verifier
)
```

## Using the Access Token to make a request to BCDE

Once we have the Access Token, we can use our authenticated session to make
requests.

We can make `GET`, `POST`, `PUT` or `DELETE` requests to endpoints that are
supported, but we need to ensure we include a signed `Authorization` header that
includes the Access Token, `oauth_token`.

An example of this is:

```http request
Authorization:
	OAuth
		oauth_nonce="116324165200041628961561373783",
		oauth_timestamp="1561373783",
		oauth_version="1.0",
		oauth_signature_method="HMAC-SHA1",
		oauth_consumer_key="ecb03487-b4fe-4e0b-9766-f40fbe01292b",
		oauth_token="6f3dfad3-3853-4c51-971f-07cffacc2c67",
		oauth_signature="yrTi5yVR52MCYSqL%2FedNvSp7D3U%3D"
```

In the final step of our example, we simply make a `GET` request to the root of
the API (which must have a trailing slash: `/api/`).

This library takes care of ensuring the `GET` request contains the
`Authorization` header information, including the *authenticated* `oauth_token`.

Once the server responds, we print out the JSON that is returned.

In our example we decode the transmitted JSON to a Python data structure and
store in a variable, `root`, then print it out.  We've chosen to turn it back
into JSON for printing (but added some readability to the output, instead of
dumping the exact bytes from the wire).

```python
root = bc_session.get(
    "%s/api/" % (SERVER_URI, )
).json()

print json.dumps(root, indent=4, sort_keys=True)
```

In most of the other Python examples, we have split this step out to the
`authenticate` function in the `ws_utils` package.

In order to get back an authenticated `OAuth1Session` instance,
`import ws_utils`, get the config and call the `authenticate` function:

```python
import ws_utils
CONFIG = ws_utils.get_config()
bc_session = ws_utils.authenticate(CONFIG)
```

This function wraps up the functionality to save the authenticated Access Token
(`oauth_token` and `oauth_token_secret`) to file and loads it back again to
stop you from having to go through the authentication process every time you
run an example.

## <a name="sample-output-from-the-script"></a> Sample output from the script

* See [authenticate_debug.log](../python/data/authenticate_debug.log) for an
example of the full HTTP headers that are sent.
* See [authenticate_payload.txt](../python/data/authenticate_payload.txt) for
an example of the JSON returned.

## <a name="notes"></a> Notes

We only support authorisation through the
[Authorization Header](https://tools.ietf.org/html/rfc5849#section-3.5.1) and
not a form-encoded body/request URI.  If you are using the
[Postman API Client](https://www.postman.com/product/api-client), in the
Authorization tab after selecting
[OAuth 1.0](https://learning.postman.com/docs/postman/sending-api-requests/authorization/#oauth-10),
you will need to set the "Add authorization data to" setting to "Request Headers" as the
current default of "Request Body / Request URL" is form-encoded body/request
URI, which we do not support.
