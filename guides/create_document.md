# Creating a Document

This guide describes the functionality in the example script
[create_document.py](../python/create_document.py).

## TOC

 * [Before you start](#before-you-start)
    * [Rate Limiting](#rate-limiting)
    * [Constructing URIs](#constructing-uris)
    * [Information you will need](#information-you-will-need)
 * [Get the URI for the destination container's contents](#get-the-uri-for-the-destination-containers-contents)
 * [Add a document to the Space](#add-a-document-to-the-space)
 * [Sample output from the script](#sample-output-from-the-script)
 * [Notes](#notes)

## <a name="before-you-start"></a> Before you start

If you are running the python example script while following along, make sure
to read the Python Examples [README.md](../python/README.md) file to ensure you
have the required libraries installed and the correct config set.

Additionally, this example does not deal with authentication, so you should
refer to [authenticate.md](authenticate.md) if you have any problems
authenticating.  We will use the `ws_utils.authenticate()` function as a
starting point for any request as discussed in the
[Using the Access Token to make a request to BCDE](../guides/authenticate.md#using-the-access-token-to-make-a-request-to-bcde)
section.

### <a name="rate-limiting"></a> Rate Limiting

Requests made to the BCDE API may be rate limited. All responses from the BCDE
API should be checked for a status code of `429`, on receiving such a status
code the request should be retried but not before the number of seconds
specified in the `Retry-After` header have passed.

### <a name="constructing-uris"></a> Constructing URIs

In our example script we construct URIs at one point - we append the hardcoded
string `/contents` to the `container_uri` rather than immediately making a `GET`
to that endpoint and discovering the URI for the contents endpoint by inspecting
the `contents` key.  We suggest that you discover them rather than construct
them - see [REST and Discoverability](../python/README.md#rest-and-discoverability).

### Information you will need

 * The BCDE ID of the container in which you are going to create the document.

The first thing we do in this example is take the BCDE ID for the artifact and
lookup the URI.

When actually writing an application, you might already have this URI if you
have used the API to create the container that you are adding the document to.

Alternatively, you may have found the container by doing a simple search or
browsed through to it from the root endpoint, actions that include URIs for
the artifacts they return.

## <a name="get-the-uri-for-the-destination-containers-contents"></a> Get the URI for the destination container's contents

Once we have retrieved the config and authenticated, the first thing we do is
convert a BCDE ID to a URI.  To do this we need to make a `POST` request to the
`lookup_uri` endpoint, but we must first discover that endpoint from the root
endpoint.  See
[Accessing the Web Services root](create_project.md#accessing-the-web-services-root)
in the [create_project.md](create_project.md) guide for more information on the
root.

By passing through an array of BCDE IDs in the body of the request to the
`lookup_uri` endpoint, a JSON object is returned with the URIs for those BCDE IDs.
The object uses string representations of the BCDE IDs as keys, meaning we can
lookup the same BCDE ID we passed through in order to get the URI for that BCDE ID.

In our example, we simply `POST` a list to the endpoint:

```python
params = [SPACE_ID,]
artifact_lookup = bc_session.post(
    artifact_lookup_uri,
    json=params
)
```

We call `.json()`  on the response to get a python dictionary representation
for the JSON object response and store this in the variable `artifact_lookup`.

So, with an input of `694`, we get the following back:

```python
{
    "694": "/api/artifacts/project-694"
}
```

We look up our Space URI by calling:

```python
artifact_lookup[str(SPACE_ID)]
```

Then add `/contents` to the end of that to get the contents endpoint.

All together, the example for this step is:

```python
root_uri = "%s/api/" % (SERVER_URI, )
root = bc_session.get(
    root_uri
).json()
returned_lookup_uri = root.get('services', {}).get('lookup_uri', None)
artifact_lookup_uri = "%s%s" % (
    SERVER_URI,
    returned_lookup_uri,
)

params = [SPACE_ID,]
artifact_lookup = bc_session.post(
    artifact_lookup_uri,
    json=params
).json()

container_uri = '%s%s' % (SERVER_URI, artifact_lookup[str(SPACE_ID)])
contents_uri = '%s/contents' % (container_uri, )
```

## <a name="add-a-document-to-the-space"></a> Add a document to the Space

We add a document to the Space by making a `POST` request to the contents
endpoint for the Space.

Whereas we passed through an array of BCDE IDs to the `lookup_uri` endpoint, here
we pass through a JSON object in the body with the following keys:
* `name` - The name of the new document.
* `content_type` - The content type (MIME).
* `content_encoding` - The content encoding.  The two supported encodings are `ascii` and `base64`.
* `content` - The actual content, encoded using the encoding in `content_encoding`.
* `kind` - Type of the artifact we're creating.  We use 'document' to create a document.

This returns a JSON object with the URI of the newly created document.

In our example we do this by making a `POST` request as follows:

```python
response = bc_session.post(
    contents_uri,
    json={
        'name': "test.txt",
        'content_type': 'text/plain',
        'content_encoding': 'ascii',
        'content': "TEST CONTENT".encode('ascii'),
        'kind': 'document',
    },
)
```

## <a name="sample-output-from-the-script"></a> Sample output from the script

* See [create_document_debug.log](../python/data/create_document_debug.log) for an
example of the full HTTP headers that are sent.
* See [create_document_payload.txt](../python/data/create_document_payload.txt) for
an example of the JSON returned.

## <a name="notes"></a> Notes

There was a fixed 32MB upload limit when using the Web Services.  This will be
a configurable limit in the following bugfix versions:
* BCDE 7.1.5,
* BCDE 7.2.3
* BCDE 7.3.1

It will be available as standard from BCDE 7.4 onwards.
