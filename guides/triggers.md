# Triggers (event subscriptions)

_This documentation was written against BCDE version 7.5._

With "triggers", BCDE can be configured to make an HTTP POST request to an
external callback service when certain events happen.

A callback URL would be configured alongside a list of events.  Every time one
of the listed events occurs, a request will be made by BCDE to the external
callback URL indicating which event was set and which object was changed.  If
multiple subscribed events are set at the same time, multiple requests will be
made (one for each event).

Contents of this guide:

* [Subscribed events](#subscribed-events)
* [Subscribed object types](#subscribed-object-types)
* [Callback requirements](#callback-requirements)
* [Authentication](#authentication)
* [Message content](#message-content)
* [Limitations](#limitations)

## Registering

Register the callback URL, and the events that will trigger requests to it, by
contacting BCDE support.  This configuration cannot be changed through the
BCDE interface.  BCDE support will need to know:

* The events that will trigger the requests (see [Subscribed
  events](#subscribed-events)).
* The types of objects that will trigger the requests (see [Subscribed object
  types](#subscribed-object-types)).
* The callback URL (see [Callback requirements](#callback-requirements)).
* A shared secret for HMAC message authentication (see
  [Authentication](#authentication)).

## Subscribed events

Whenever state changes for an object (such as a Space or Document) in the BCDE
instance, an event is set.  The event has a name reflecting the type of
change, and is associated with one or more objects (for example, a creation
event will be associated with the object which was created, as well as the
container it was created in).

When subscribing to a particular event, the usual practice is to subscribe for
the object which the event happened "to" (for example, a Document that was
moved to a new container) but it is also possible to choose to subscribe for an
object which had a different association (for example, the Folder into which an
object was moved).  This is relevant when using an object type filter as part
of the subscription definition (see [Subscribed object
types](#subscribed-object-types)).

The following is a list of events which are often of interest.  It is not
exhaustive; contact BCDE support if you need to be aware of events not listed
here.

* `RenameEvent` / `CorrectNameEvent`: set when an object is renamed.
  `CorrectNameEvent` is set only when the object is immutable (for example,
  when a Document has been Issued and cannot ordinarily be changed). Otherwise
  `RenameEvent` is set.  If you just want to know that an object's name has
  changed, you should subscribe to both.
* `ShortRenameEvent`: set when an object's "short name" is changed.  (Not all
  objects have a short name.)
* `ChangeDescEvent` / `CorrectDescEvent`: set when an object's description is
  changed.
* `MetadataTypeChangeEvent` / `MetadataTypeCorrectedEvent`: set when an
  object's "object type" is changed.  This refers to the type which governs the
  available metadata fields, _not_ a Document's MIME type.
* `MetadataEditEvent` / `MetadataCorrectedEvent`: set when an object's custom
  metadata is changed.  Note that if a system field such as the object name is
  changed, a specific event will be set for that (such as `RenameEvent`)
  instead of the metadata event - even if the name was changed through "Edit
  Metadata".  In other words, those more specific events are based on the
  fields being changed, not the UI operation that was used to change them.
* `ChangeEvent`: set for other general changes to an object's attributes,
  including changes to a Document's MIME type.
* `CreateEvent` / `CopyEvent` / `ReviseEvent`: set when a new object is
  created.
  * `CreateEvent` is the usual event, set when a user creates an object through
    the "Add" menu.
  * `CopyEvent` is set when the new object is a copy of an existing object
    (usually created by selecting an object then choosing "Copy").  There will
    usually later be a `DropEvent` when the user pastes the new object into its
    intended location.
  * `ReviseEvent` is set when a new revision of a Document is created.
* `CutEvent`: set when an object is cut from its current location.  This is the
  first part of a two-part cut-and-past process, where the user cuts the object
  and it moves to their Bag temporarily.  There will usually later be a
  `DropEvent` when the user pastes the new object into its intended location.
* `DropEvent`: set when an object is pasted somewhere.
* `PlaceholderConvertEvent`: set when a placeholder is fulfilled with file
  content.
* `IssueSendEvent`: set when an Issue is sent.  (When the Issue is created,
  before being configured and sent, the usual `CreateEvent` is set.  Usually
  an Issue becomes interesting when it's been sent.)
* `IssueCompleteEvent` / `IssueAdminCompleteEvent`: set when an Issue is
  completed.
  * `IssueCompleteEvent` is set when an Issue is completed in the usual way.
  * `IssueAdminCompleteEvent` is set when an administrator intervenes to
    complete an Issue.
* `IssueCancelEvent` / `IssueAdminCancelEvent`: set when an Issue is cancelled.
  * `IssueCancelEvent` is set when an Issue is completed in the usual way.
  * `IssueAdminCancelEvent` is set when an administrator intervenes to cancel
    an Issue.
* `IssueCompleteActionEvent` / `IssueCompleteActionOnBehalfEvent` /
  `IssueAdminCompleteActionEvent`: set when a user's individual task on a
  single-response Issue is completed (when tasks are _not_ per-document).
  * `IssueCompleteActionEvent` is set when a task is completed in the usual
    way.
  * `IssueCompleteActionOnBehalfEvent` is set when a task is completed by
    somebody other than the task owner (for example, when a colleague in the
    same company responds on behalf of a recipient, where the Issue allows
    that).
  * `IssueAdminCompleteActionEvent` is set when an administrator intervenes to
    complete a task.
* `IssueCompleteActionOnDocumentEvent` /
  `IssueCompleteActionOnBehalfOnDocumentEvent` /
  `IssueAdminCompleteActionOnDocumentEvent`: set _on an Issue_ when a user's
  task for a Document on that Issue is completed.  This is set on the Issue at
  the same time as a `...ForDocEvent` is set on the Document.  (The name of the
  event is somewhat confusing when compared to `...OnDocumentEvent`, and these
  definitions may even seem to be the wrong way around, but they are correct.)
* `IssueCompleteActionForDocEvent` / `IssueCompleteActionOnBehalfForDocEvent` /
  `IssueAdminCompleteActionForDocEvent`: set _on a Document_ when a user's task
  for that Document on an Issue is completed.  This is set on the Document at
  the same time as an `...OnDocumentEvent` is set on the Issue.  (The name of
  the event is somewhat confusing when compared to `...ForDocEvent`, and these
  definitions may even seem to be the wrong way around, but they are correct.)
* `ForwardIssueEvent`: set when a user's task is delegated to another user,
  adding that other user to the Issue distribution.
* `IssueForwardEvent`: set when Documents are re-issued (sent on a new Issue,
  based on an existing one).

## Subscribed object types

If you are interested in particular types of object (such as Documents, Issues
or Spaces) then you should use an object type filter on the subscription, so
that BCDE can reduce the number of requests it makes to the callback URL.

For Documents you can also filter to individual MIME types (exact types only;
wildcards are not supported).

In either case, mention these requirements when contacting BCDE to set up the
event subscription.

## Callback requirements

The service listening on the callback URL must accept a POST request from BCDE,
transmitted over SSL/TLS.  (The registered URL must start with `https://` for
clarity.)  It must respond with a 200-series HTTP status code to acknowledge
receipt, and should respond quickly; we recommend saving the message details
in a queue to be processed "offline", so as not to delay sending the
acknowledgement or to block receipt of further messages.

The service should have high availability as events can be set (and therefore
messages sent) at any time.  Don't assume that having users in a single time
zone means that events will be set within nominal working hours for that time
zone: apps and automated processes may cause events to be set at any time of
the day.

If BCDE is unable to contact the callback URL, or receives a non-2xx response
code, it will retry the message after a short wait: the first retry will be no
sooner than one minute later, the second retry no sooner than two minutes
after that, and so on, doubling the wait time for each subsequent attempt
(but capped at 8 hours).  The request will be attempted a total of 30 times
(over a period of at least 7 days), after which if it has still not
succeeded, ***it will be discarded***.

BCDE will generally attempt to make requests in an order corresponding to the
order in which events were set, but this is not guaranteed and should not be
relied upon.  Network delays may cause requests to arrive in a different order
than they were sent in, and retries (as outlined above) can have a big effect
on the order.  In particular, beware that BCDE may send two or more requests
concurrently and the callback server must be prepared to handle that.

## Authentication

The shared secret, configured as part of registering the event subscription,
will be used to sign the request with an HMAC digest (see [RFC
2401](https://datatracker.ietf.org/doc/html/rfc2104.html)).  This is
implemented as a SHA-256 digest of the request body, represented as a
hexadecimal sequence (with lower-case letters), and communicated as the value
of a `digest` HTTP header.

## Message content

For each subscribed event which is set, BCDE will make a POST request to a URI
that you've supplied to BCDE support.  The request body will be encoded as
`application/x-www-form-urlencoded` and will contain some key-value pairs.

Very few details of the event are included in the request.  You should make a
request to the `event` URI given in the trigger request (using whatever
authentication your app has registered for accessing BCDE web services,
independent of this trigger mechanism).

  * `event`: the URI of the event that was set (relative to the `server_root`
    value).  This should be followed to get the full details of the event.
  * `eventType`: the name of the event that triggered the request.
  * `id`: the internal ID of a BCDE object involved in the event.  Where more
    than one object was involved, this is usually the object that the event
    happened "to" (but see [Subscribed events](#subscribed-events): if you've
    specified a different association as part of the subscription, the object
    will match that configuration).  BCDE internal IDs are positive integers.
  * `kind`: the type of object represented by `id`.  The values possible here
    are those that may appear in the `kind` attribute in BCDE web services.
  * `nonce`: a unique value sent with every request.  Since the value changes
    with each request, you may use this to guard against replay attacks or to
    disregard duplicate messages caused by network problems.
  * `server_root`: the base URL of the BCDE server.  Join this with the value
    given by `event` to get the full web service URI of the event.
  * `timestamp`: the time at which _the request was sent_.  This is usually
    close to the time that the event was set, but it isn't guaranteed (for
    example, if a request failed and was later retried).  To get the time of
    the event itself, visit the web service URI for the event.
  * `uri`: the web service URI of the object represented by `id`.
  * `username`: the login name of the user who caused the event to be set.
  * `version`: when the keys of the request are as described here, the version
    number is 4.  If the version number is higher, there may be more keys in
    the message (and this documentation is out of date with respect to the BCDE
    server).  If the version number is lower, some of the keys may be missing.

It is important to remember that details of the change are not included in the
request, and you should use BCDE web services to get more detail about the
event, and about object states.  However in general, events do not record
(and therefore do not show in web services) the exact object changes that
caused the event.  Moreover, it's possible that the state of an object has
changed since the subscribed-event request was triggered.  Therefore you
should write your application to work with the current state of any interesting
objects (as read from web services), with events providing a reason to
re-examine that state, rather than assuming that some event necessarily implies
a particular state.

## Limitations

Subscriptions to events are _server-wide_; it is not possible to subscribe just
within a single Space, for example.  On a large BCDE server this can lead to a
lot of triggered requests, which your application must be ready to handle.  If
possible, reducing the coverage of the subscription by carefully choosing the
event names and object types is recommended.

The request made to the callback URL contains information from the BCDE server;
the information is limited (as discussed above), but it is there. Whereas the
full information (accessed through BCDE web services) requires authentication
at the time of reading, the trigger requests can't authenticate the
destination callback URL.  Therefore careful thought is required before an
event subscription is registered with a BCDE server, to be sure that the
implementer of the callback service is a trusted party.

When the external app (itself responding to BCDE accessing the callback URL)
attempts to read event information through web services, it will have to
authenticate with the BCDE server.  The user it authenticates as will need to
be granted certain access rights to be able to read information.

* To read an event's information, it will need "info" access (under "Get Ext"
  in the BCDE access rights UI) for any of the objects involved in the event.
  (Events don't have their own access control; access is determined based on
  the objects involved.)
* To read an object's information, it will need "get" or "info" access (under
  "Get" and "Get Ext" respectively in the BCDE access rights UI).
