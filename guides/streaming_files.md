# Streaming Files

This guide describes the functionality from example script
[create_a_big_document.py](../python/create_a_big_document.py).

It allows putting documents into the product via API

## TOC

 * [Before you start](#before-you-start)
 * [Preparing The Script](#preparing-the-script)
 * [Script Usage](#script-usage)

## <a name="before-you-start"></a> Before you start

If you are running the Python example script while following along, make sure
to read the Python Examples [README.md](../python/README.md) file to ensure you
have the required libraries installed and the correct config set.

This example does not deal with authentication, so you should
refer to [authenticate.md](authenticate.md#before-you-start) for
registering an application.

The script takes authentication related variables from
`python/config.default.json`.

## <a name="script-usage"></a> Script Usage

```bash
python python/create_a_big_document.py <FILE_PATH>
```

You will be asked to authorize the application in the web browser and
provide the verification code received during the authorization process.

Afterwards, `FILE_PATH` will be uploaded into BCDE.

`FILE_PATH` must indicate a file.
