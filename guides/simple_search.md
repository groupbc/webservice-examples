# Performing a Simple Search

This guide describes the functionality in the example script
[simple_search.py](../python/simple_search.py).

## TOC

 * [Before you start](#before-you-start)
    * [Rate Limiting](#rate-limiting)
    * [Keyword language](#keyword-language)
 * [Finding the Simple Search Service](#finding-the-simple-search-service)
 * [Performing the Search](#performing-the-search)
 * [Processing the Results](#processing-the-results)
 * [Sample output from the script](#sample-output-from-the-script)

## <a name="before-you-start"></a> Before you start

If you are running the python example script while following along, make sure
to read the Python Examples [README.md](../python/README.md) file to ensure you
have the required libraries installed and the correct config set.

Additionally, this example does not deal with authentication, so you should
refer to [authenticate.md](authenticate.md) if you have any problems
authenticating.  We will use the `ws_utils.authenticate()` function as a
starting point for any request as discussed in the
[Using the Access Token to make a request to BCDE](../guides/authenticate.md#using-the-access-token-to-make-a-request-to-bcde)
section.

### <a name="rate-limiting"></a> Rate Limiting

Requests made to the BCDE API may be rate limited. All responses from the BCDE
API should be checked for a status code of `429`, on receiving such a status
code the request should be retried but not before the number of seconds
specified in the `Retry-After` header have passed.

### <a name="keyword-language"></a> Keyword language

We construct the simple search in this example using the keyword language that
BCDE supports for searching.  In our example, we will search for items by
the value of a specific metadata field and an artifact by its class.

For more information on keyword language queries, see section 6.7 of the BCDE
help.

## <a name="finding-the-simple-search-service"></a> Finding the Simple Search Service

Once we have retrieved the config and authenticated, the first thing we do is
discover the URI for the simple_search endpoint in the root endpoint.  See
[Accessing the Web Services root](create_project.md#accessing-the-web-services-root)
in the [create_project.md](create_project.md) guide for more information on the
root.

The URI for the simple search service is given as the `simple_search` property
in the `services` section - `/api/services/simple_search?` in the sample root
JSON.

## <a name="performing-the-search"></a> Performing the Search

Performing the search is as simple as making a `GET` request to the simple
search endpoint, giving your keyword language query as the `klang` parameter.

For example, to query for everything where the Metadata Metafield ID of _1202_
has a value of _Two_:

```
/api/services/simple_search?klang=md_1202%3DTwo
```

The main complexity will be related to constructing your keyword language term.
The keyword language in BCDE is very powerful, but it requires a detailed
knowledge of BCDE concepts when you wish to search for specific Metadata values.
In our example above, the ID _1202_ is actually related to the `Metafield`
ID.  The easiest way to find this is to go to the Metadata endpoint for the
artifact you are looking to find.  Then pass the value of the `metafield`
property for the field that you want to search in through to the `lookup_id`
service which will return the BCDE ID.

For example, if we look at the following partial sample of data returned from
the Metadata endpoint, we would need to use `md_3163` and `md_3397` for the
_Format_ and _Unique Asset Id_ fields respectively.

```json
{
    "values": {
        "general_md": {
            "3163": {
                "metafield": "/api/objects/metafields/metadatametafield-3163",
                "datatype": "/api/metadata_datatypes/dropdown",
                "listing_column": {
                    "identifier": "3163",
                    "type": "general_md"
                },
                "datum": {
                    "detail": {
                        "items": [
                            "Accordion z-fold"
                        ]
                    },
                    "basic": [
                        "Accordion z-fold"
                    ]
                },
                "field": "/api/artifacts/documenttype-3124/field_sets/metadatafieldsetname-3114/fields/metadatametafield-3163",
                "is_protected": 0,
                "error": null,
                "is_required": false,
                "field_title": "Format",
                "is_valid": true
            },
            "3397": {
                "metafield": "/api/objects/metafields/metadatametafield-3397",
                "datatype": "/api/metadata_datatypes/string",
                "listing_column": {
                    "identifier": "3397",
                    "type": "general_md"
                },
                "datum": {
                    "detail": {
                        "text": "ABC-000000002"
                    },
                    "basic": "ABC-000000002"
                },
                "field": "/api/artifacts/schematype-3113/field_sets/metadatafieldsetname-3114/fields/metadatametafield-3397",
                "is_protected": 0,
                "error": null,
                "is_required": false,
                "field_title": "Unique Asset Id",
                "is_valid": true
            }
        }
    }
}
```

In our example, we construct the `GET` request as follows:

```python
params = {
    'klang': KLANG_SEARCH_QUERY,
}
search_results = bc_session.get(
    search_results_uri,
    params=params,
)
```

Where the variables will contain something like:
```python
print KLANG_SEARCH_QUERY
# Prints "md_1202=Two"
print search_results_uri
# Returns "https://dev.withbc.com/api/services/simple_search?"
```

## <a name="processing-the-results"></a> Processing the Results

Here is an example response from a simple search query:

```json
{
   "query_last_run":"2016-04-20T14:47:43.350469+00:00",
   "is_total_exact":true,
   "is_search":true,
   "next":null,
   "start":0,
   "sort_by":[
      [
         "container_and_name",
         true,
         null
      ],
      [
         "id",
         true,
         null
      ]
   ],
   "limit":25,
   "view_preference":"/api/artifacts/searchbag-1284/view_preference",
   "total":1,
   "previous":null,
   "query_refresh":"/api/services/simple_search?klang=md_1202%3DTwo&refresh_if_older=0",
   "contents":[
      {
         "kind":"project",
         "name":"Example Project",
         "descr":"",
         "created":{
            "user":"/api/artifacts/user-229",
            "login_name":"admin",
            "full_name":"Administrator",
            "time":"2016-04-20T14:24:45.570427+00:00"
         },
         "web_ui_link_lp":{
            "url":"http://assure.docker.local/bc/bc.cgi/0/234?op=lp&id=1207",
            "title":"lp",
            "can_perform":true,
            "name":"lp",
            "requires_entry":false
         },
         "uri":"/api/artifacts/project-1207",
         "modified":{
            "user":"/api/artifacts/user-229",
            "login_name":"admin",
            "full_name":"Administrator",
            "time":"2016-04-20T14:24:45.570427+00:00"
         },
         "web_ui_link_get":{
            "url":"http://assure.docker.local/bc/bc.cgi/0/1207",
            "title":"Get",
            "can_perform":true,
            "name":"get",
            "requires_entry":false
         }
      }
   ],
   "first":"/api/services/simple_search?page=1&klang=md_1202%3DTwo"
}
```

The most important property is the `contents` which is an array of objects
found in the search. Each of these will include some basic information and a `uri`
property for accessing the full representation of the result.

If more than a single page (25 items) of results are found in the search, additional
links will be exposed on the response for retrieving the additional pages.

In our example, we check to see if there are more pages by checking to see
if the `next` property has a value:

```python
if search_results.get("next"):
```

Additionally, we check the `is_total_exact` property to see if the `total`
property contains an exact count or just an approximation.  This is just
like in a BCDE listing and is done because further pages may contain results
which would be filtered out by access rights, and we do not take this into
consideration in the total until we go through all pages.

```python
if not search_results.get("is_total_exact"):
```

We put all of this together to output the count of the results:

```python
is_exact_string = ''
if not search_results.get("is_total_exact"):
    is_exact_string = ' about'

more_pages_available_string = ''
if search_results.get("next"):
    more_pages_available_string = '  More pages available.'

print "\n\nSearch on %s" % (search_results_uri, )

print "Found%s %d result(s).%s" % (
    is_exact_string,
    search_results.get("total", 0),
    more_pages_available_string,
)
```

Once we have done that, we iterate through the first page of results and print
out the name and URI for each artifact found:

```python
print "Showing %d result(s):" % (len(search_results.get("contents", [])), )
for index, content in enumerate(search_results.get("contents", [])):
    print " * %d. %s : %s" % (
        index + 1,
        content.get("name", ""),
        content.get("uri", "")
    )
```

## <a name="sample-output-from-the-script"></a> Sample output from the script

* See [simple_search_debug.log](../python/data/simple_search_debug.log) for an
example of the full HTTP headers that are sent.
* See [simple_search_payload.txt](../python/data/simple_search_payload.txt) for
an example of the JSON returned.
