# Getting and Setting Metadata

This guide describes the functionality in the example script
[set_metadata.py](../python/set_metadata.py).

## TOC

 * [Before you start](#before-you-start)
    * [Rate Limiting](#rate-limiting)
    * [Information you will need](#information-you-will-need)
 * [Getting the metadata](#getting-the-metadata)
 * [Setting the metadata](#setting-the-metadata)
 * [Key-value pairs for fields by type](#key-value-pairs-for-fields-by-type)
 * [Sample output from the script](#sample-output-from-the-script)

## <a name="before-you-start"></a> Before you start

If you are running the python example script while following along, make sure
to read the Python Examples [README.md](../python/README.md) file to ensure you
have the required libraries installed and the correct config set.

Additionally, this example does not deal with authentication, so you should
refer to [authenticate.md](authenticate.md) if you have any problems
authenticating.  We will use the `ws_utils.authenticate()` function as a
starting point for any request as discussed in the
[Using the Access Token to make a request to BCDE](../guides/authenticate.md#using-the-access-token-to-make-a-request-to-bcde)
section.

This guide does not deal with setting the Metadata type, but there is an
example script for that: [set_metadata_type.py](../python/set_metadata_type.py).

### <a name="rate-limiting"></a> Rate Limiting

Requests made to the BCDE API may be rate limited. All responses from the BCDE
API should be checked for a status code of `429`, on receiving such a status
code the request should be retried but not before the number of seconds
specified in the `Retry-After` header have passed.

### Information you will need

 * The BCDE ID of the artifact whose metadata you want to get or set.  The first
thing we do is lookup the URI for the artifact from its BCDE ID.  When actually
writing an application, you might already have the URI rather than the BCDE ID
from when you created the artifact, looked for it by using the simple
search service or even browsed through to it from the root endpoint.

 * The BCDE ID of the artifact whose metadata you want to get or set.

The first thing we do in this example is take the BCDE ID for the artifact and
lookup the URI.

When actually writing an application, you might already have this URI if you
have used the API to create this artifact.

Alternatively, you may have found the artifact by doing a simple search or
browsed through to it from the root endpoint, actions that return URIs for
the artifacts they return.

## <a name="getting-the-metadata"></a> Getting the metadata

Just like when [creating a document](create_document.md), the first thing we
need to do is to get the URI for the artifact for which we want to get the
metadata.  See the section
[Get the URI for the destination container's contents](create_document.md#get-the-uri-for-the-destination-containers-contents),
as we use the same technique.

When we make a `GET` request to the artifact uri we get a representation of
the artifact. This has a `metadata` property that is the uri for the
metadata endpoint for this artifact.

```json
{
   "current_type":"/api/artifacts/project-1419/types/schematype-1194",
   "members":"/api/artifacts/project-1419/members",
   "web_ui_link_lp":{
      "url":"http://assure.docker.local/bc/bc.cgi/0/234?op=lp&id=1419",
      "title":"lp",
      "can_perform":true,
      "name":"lp",
      "requires_entry":false
   },
   "project_image":"/api/artifacts/project-1419/project_image",
   "new_object_types":{
      "project":"/api/artifacts/project-1419/types_for/project",
      "folder":"/api/artifacts/project-1419/types_for/folder",
      "document":"/api/artifacts/project-1419/types_for/document",
      "issue":"/api/artifacts/project-1419/types_for/issue",
      "collection":"/api/artifacts/project-1419/types_for/collection"
   },
   "descr":"Example to demonstrate webservices",
   "access_rights":"/api/artifacts/project-1419/access_rights",
   "contents":"/api/artifacts/project-1419/contents",
   "web_ui_link_get":{
      "url":"http://assure.docker.local/bc/bc.cgi/0/1419",
      "title":"Get",
      "can_perform":true,
      "name":"get",
      "requires_entry":false
   },
   "containers":[
      "/api/artifacts/home-234"
   ],
   "metadata":"/api/artifacts/project-1419/metadata",
   "owners":"/api/artifacts/project-1419/owners",
   "last_modified":"2016-04-28T09:31:45.216520+00:00",
   "view_preference":"/api/artifacts/project-1419/view_preference",
   "services":{
      "lookup_user":"/api/artifacts/project-1419/services/lookup_user",
      "navigation":"/api/artifacts/project-1419/services/navigation"
   },
   "path":[
      {
         "kind":"home",
         "web_ui_link_get":{
            "url":"http://assure.docker.local/bc/bc.cgi/0/234",
            "title":"Get",
            "can_perform":true,
            "name":"get",
            "requires_entry":false
         },
         "name":"Home",
         "uri":"/api/artifacts/home-234"
      }
   ],
   "kind":"project",
   "name":"Example Project",
   "created":{
      "user":"/api/artifacts/user-229",
      "login_name":"admin",
      "full_name":"Administrator",
      "time":"2016-04-28T09:31:18.769680+00:00"
   },
   "modified":{
      "user":"/api/artifacts/user-229",
      "login_name":"admin",
      "full_name":"Administrator",
      "time":"2016-04-28T09:31:18.769680+00:00"
   },
   "project":"/api/artifacts/project-1419",
   "web_ui_links":"/api/artifacts/project-1419/web_ui_links",
   "issue_location":"/api/artifacts/project-1419/issues",
   "assignable_types":"/api/artifacts/project-1419/assignable_types"
}
```

When we make a `GET`  request to the metadata endpoint, we get a representation
of the artifact's metadata, including the current values and details of all fields.

```json
{

    "assignable_types": "/api/artifacts/project-1331/assignable_types",
    "explicit_values": true,
    "file_metadata_errors": null,
    "general_metadata_field_order": [
        {
            "identifier": "description",
            "type": "system"
        },
        {
            "identifier": "name",
            "type": "system"
        },
        {
            "identifier": "86511",
            "type": "general_md"
        },
        {
            "identifier": "86515",
            "type": "general_md"
        }
    ],
    "is_valid": true,
    "type": "/api/artifacts/project-1331/type",
    "type_id": "/api/artifacts/project-1331/types/schematype-1010",
    "values": {
        "file_md": { },
        "general_md": {
            "86511": {
                "datatype": "/api/metadata_datatypes/multiselect",
                "datum": {
                    "basic": null,
                    "detail": { }
                },
                "error": null,
                "field": "/api/artifacts/schematype-1010/field_sets/metadatafieldsetname-1011/fields/metadatametafield-86511",
                "field_title": "PSM Appended",
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "86511",
                    "type": "general_md"
                },
                "metafield": "/api/objects/metafields/metadatametafield-86511"
            },
            "86515": {
                "datatype": "/api/metadata_datatypes/multiselect",
                "datum": {
                    "basic": null,
                    "detail": { }
                },
                "error": null,
                "field": "/api/artifacts/schematype-1010/field_sets/metadatafieldsetname-1011/fields/metadatametafield-86515",
                "field_title": "PSM Override",
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "86515",
                    "type": "general_md"
                },
                "metafield": "/api/objects/metafields/metadatametafield-86515"
            }
        },
        "issue_response": { },
        "system": {
            "completed": {
                "datatype": "/api/metadata_datatypes/system_datetime_completed",
                "datum": {
                    "basic": null,
                    "detail": { }
                },
                "error": null,
                "field": null,
                "field_title": null,
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "completed",
                    "type": "system"
                },
                "metafield": null
            },
            "created": {
                "datatype": "/api/metadata_datatypes/system_datetime_created",
                "datum": {
                    "basic": "29/04/15 10:23:50",
                    "detail": {
                        "datetime": "2015-04-29T10:23:50.048268+00:00"
                    }
                },
                "error": null,
                "field": null,
                "field_title": null,
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "created",
                    "type": "system"
                },
                "metafield": null
            },
            "creator": {
                "datatype": "/api/metadata_datatypes/system_user_creator",
                "datum": {
                    "basic": "admin",
                    "detail": {
                        "full_name": "admin",
                        "login_name": "admin",
                        "user": "/api/artifacts/user-204"
                    }
                },
                "error": null,
                "field": null,
                "field_title": null,
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "creator",
                    "type": "system"
                },
                "metafield": null
            },
            "description": {
                "datatype": "/api/metadata_datatypes/description",
                "datum": {
                    "basic": "A description",
                    "detail": {
                        "is_html": false,
                        "text": "A description"
                    }
                },
                "error": null,
                "field": null,
                "field_title": "Description",
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "description",
                    "type": "system"
                },
                "metafield": "/api/objects/metafields/metadatametafield-73"
            },
            "doc_size": {
                "datatype": "/api/metadata_datatypes/system_doc_size",
                "datum": {
                    "basic": null,
                    "detail": { }
                },
                "error": null,
                "field": null,
                "field_title": null,
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "doc_size",
                    "type": "system"
                },
                "metafield": null
            },
            "issue_sender": {
                "datatype": "/api/metadata_datatypes/system_user_issue_sender",
                "datum": {
                    "basic": null,
                    "detail": { }
                },
                "error": null,
                "field": null,
                "field_title": null,
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "issue_sender",
                    "type": "system"
                },
                "metafield": null
            },
            "issue_state": {
                "datatype": "/api/metadata_datatypes/system_issue_state",
                "datum": {
                    "basic": null,
                    "detail": { }
                },
                "error": null,
                "field": null,
                "field_title": null,
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "issue_state",
                    "type": "system"
                },
                "metafield": null
            },
            "last_modified": {
                "datatype": "/api/metadata_datatypes/system_datetime_last_modified",
                "datum": {
                    "basic": "16/12/15 21:31:39",
                    "detail": {
                        "datetime": "2015-12-16T21:31:39.722242+00:00"
                    }
                },
                "error": null,
                "field": null,
                "field_title": null,
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "last_modified",
                    "type": "system"
                },
                "metafield": null
            },
            "listing_type": {
                "datatype": "/api/metadata_datatypes/system_listing_type",
                "datum": {
                    "basic": "Project",
                    "detail": {
                        "class_label": "Project",
                        "mime_label": null,
                        "mime_type": null
                    }
                },
                "error": null,
                "field": null,
                "field_title": null,
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "listing_type",
                    "type": "system"
                },
                "metafield": null
            },
            "location": {
                "datatype": "/api/metadata_datatypes/system_location",
                "datum": {
                    "basic": [
                        "Home"
                    ],
                    "detail": {
                        "path": [
                            {
                                "object": "/api/artifacts/home-209",
                                "web_ui_link_get": {
                                    "can_perform": true,
                                    "name": "get",
                                    "requires_entry": false,
                                    "title": "Get",
                                    "url": "https://centos7/bc/bc.cgi/0/209"
                                },
                                "web_ui_link_lp": {
                                    "can_perform": true,
                                    "name": "lp",
                                    "requires_entry": false,
                                    "title": "lp",
                                    "url": "https://centos7/bc/bc.cgi/0/209?op=lp&id=209"
                                }
                            }
                        ]
                    }
                },
                "error": null,
                "field": null,
                "field_title": null,
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "location",
                    "type": "system"
                },
                "metafield": null
            },
            "metadata_type": {
                "datatype": "/api/metadata_datatypes/system_metadata_type",
                "datum": {
                    "basic": "Asset Schema",
                    "detail": {
                        "object": "/api/artifacts/schematype-1010",
                        "web_ui_link_get": {
                            "can_perform": true,
                            "name": "get",
                            "requires_entry": false,
                            "title": "Get",
                            "url": "https://centos7/bc/bc.cgi/0/1010"
                        },
                        "web_ui_link_lp": {
                            "can_perform": true,
                            "name": "lp",
                            "requires_entry": false,
                            "title": "lp",
                            "url": "https://centos7/bc/bc.cgi/0/1004?op=lp&id=1010"
                        }
                    }
                },
                "error": null,
                "field": null,
                "field_title": null,
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "metadata_type",
                    "type": "system"
                },
                "metafield": null
            },
            "name": {
                "datatype": "/api/metadata_datatypes/filename",
                "datum": {
                    "basic": "New Project Workspace",
                    "detail": {
                        "text": "New Project Workspace"
                    }
                },
                "error": null,
                "field": null,
                "field_title": "Name",
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "name",
                    "type": "system"
                },
                "metafield": "/api/objects/metafields/metadatametafield-77"
            },
            "owner": {
                "datatype": "/api/metadata_datatypes/system_user_owner",
                "datum": {
                    "basic": "admin",
                    "detail": {
                        "full_name": "admin",
                        "login_name": "admin",
                        "user": "/api/artifacts/user-204"
                    }
                },
                "error": null,
                "field": null,
                "field_title": null,
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "owner",
                    "type": "system"
                },
                "metafield": null
            },
            "parent_project": {
                "datatype": "/api/metadata_datatypes/system_parent_project",
                "datum": {
                    "basic": "New Project Workspace",
                    "detail": {
                        "object": "/api/artifacts/project-1331",
                        "web_ui_link_get": {
                            "can_perform": true,
                            "name": "get",
                            "requires_entry": false,
                            "title": "Get",
                            "url": "https://centos7/bc/bc.cgi/0/1331"
                        },
                        "web_ui_link_lp": {
                            "can_perform": true,
                            "name": "lp",
                            "requires_entry": false,
                            "title": "lp",
                            "url": "https://centos7/bc/bc.cgi/0/209?op=lp&id=1331"
                        }
                    }
                },
                "error": null,
                "field": null,
                "field_title": null,
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "parent_project",
                    "type": "system"
                },
                "metafield": null
            },
            "project_category": {
                "datatype": "/api/metadata_datatypes/project_category",
                "datum": {
                    "basic": null,
                    "detail": { }
                },
                "error": null,
                "field": null,
                "field_title": "Project Category",
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "project_category",
                    "type": "system"
                },
                "metafield": "/api/objects/metafields/metadatametafield-71"
            },
            "revision_comment": {
                "datatype": "/api/metadata_datatypes/version_comment",
                "datum": {
                    "basic": null,
                    "detail": { }
                },
                "error": null,
                "field": null,
                "field_title": "Revision Comment",
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "revision_comment",
                    "type": "system"
                },
                "metafield": "/api/objects/metafields/metadatametafield-85"
            },
            "sent_out": {
                "datatype": "/api/metadata_datatypes/system_datetime_sent_out",
                "datum": {
                    "basic": null,
                    "detail": { }
                },
                "error": null,
                "field": null,
                "field_title": null,
                "is_required": false,
                "is_valid": true,
                "listing_column": {
                    "identifier": "sent_out",
                    "type": "system"
                },
                "metafield": null
            }
        }
    },
    "writable_fields": [
        {
            "datatype": "/api/metadata_datatypes/description",
            "field": "/api/artifacts/schematype-1010/field_sets/metadatafieldsetname-1011/fields/metadatametafield-73",
            "field_title": "Description",
            "metafield": "/api/objects/metafields/metadatametafield-73",
            "suggested_value": {
                "basic": "A description",
                "detail": {
                    "is_html": false,
                    "text": "A description"
                }
            }
        },
        {
            "datatype": "/api/metadata_datatypes/filename",
            "field": "/api/artifacts/schematype-1010/field_sets/metadatafieldsetname-1011/fields/metadatametafield-77",
            "field_title": "Name",
            "metafield": "/api/objects/metafields/metadatametafield-77",
            "suggested_value": {
                "basic": "New Project Workspace",
                "detail": {
                    "text": "New Project Workspace"
                }
            }
        },
        {
            "available_values": [
                "Alpha",
                "Schema 0",
                "Schema 1",
                "Schema 2",
                "Schema 3"
            ],
            "datatype": "/api/metadata_datatypes/multiselect",
            "field": "/api/artifacts/schematype-1010/field_sets/metadatafieldsetname-1011/fields/metadatametafield-86511",
            "field_title": "PSM Appended",
            "metafield": "/api/objects/metafields/metadatametafield-86511",
            "suggested_value": {
                "basic": null,
                "detail": { }
            }
        },
        {
            "available_values": [
                "Schema 1",
                "Schema 2",
                "Schema 3"
            ],
            "datatype": "/api/metadata_datatypes/multiselect",
            "field": "/api/artifacts/schematype-1010/field_sets/metadatafieldsetname-1011/fields/metadatametafield-86515",
            "field_title": "PSM Override",
            "metafield": "/api/objects/metafields/metadatametafield-86515",
            "suggested_value": {
                "basic": null,
                "detail": { }
            }
        }
    ]

}
```

In our example, these two requests look something like the following:

```python
artifact_uri = '%s%s' % (SERVER_URI, uris[str(ARTIFACT_ID)], )
artifact = bc_session.get(
    "%s" % (artifact_uri, )
).json()

artifact_metadata_uri = "%s%s" % (SERVER_URI, artifact["metadata"], )

metadata_uri = '%s' % (artifact_metadata_uri, )
metadata = bc_session.get(
    metadata_uri
).json()
```

## <a name="setting-the-metadata"></a> Setting the metadata

The `writable_fields` attribute contains details of the fields that we can modify.
To set field values, we make a `POST` request to the metadata endpoint, passing
through a JSON object in the body of the request that represents a mapping of
fields to JSON objects, where the JSON object contains the appropriate
key-value pair for the datatype.  An example of the body content is as follows:

```json
{
    "/api/objects/metafields/metadatametafield-77": {
        "text": "This is the new name"
    },
    "/api/objects/metafields/metadatametafield-86511": {
        "items": ["Alpha", "Schema 1"]
    }
}
```

The keys for each datatype are listed in the
[Key-value pairs for fields by type](#key-value-pairs-for-fields-by-typea-key-value-pairs-for-fields-by-type)
section below.

In the example script, we iterate through the `writable fields`, compare against
the field_title, then append an entry to our `json_mapping` dictionary.
This looks something like the following:

```python
for writable_field in metadata["writable_fields"]:
    if writable_field["field_title"] == text_field_1["field_title"]:
        json_mapping[writable_field["metafield"]] = {
            "text": text_field_1["text"]
        }
    elif writable_field["field_title"] == drop_down_1["field_title"]:
        json_mapping[writable_field["metafield"]] = {
            "items": drop_down_1["items"]
        }
```

What we end up with is a python dictionary that looks something like this:

```python
json_mapping = {
	"/api/objects/metafields/metadatametafield-107":
		{"text": "Higher Education Fact Sheet Revised.pdf"},
	"/api/objects/metafields/metadatametafield-3163":
		{"items": ["Double right-angle / French fold"]},
	"/api/objects/metafields/metadatametafield-91":
		{"text": "Revised description"},
}
```

We then simply make a `POST` request to the metadata endpoint for the artifact,
and pass through the `json_mapping` dictionary as the JSON-encoded body.

```python
bc_session.post(
    metadata_uri,
    json=json_mapping
)
```

## <a href="key-value-pairs-for-fields-by-type"></a> Key-value pairs for fields by type

Each key-value pair we send in the `POST` request is identified by a metafield key.

The key required in order to set the value depends on the datatype you're trying to set.
Below are examples of the JSON objects for currently supported types.
```
Boolean: {
    "boolean": 0
}
Datetime: {
    "datetime": "2015-12-16T21:31:39.722242+00:00"
}
Date: {
    "date": "2015-12-16"
}
Int: {
    "int": 5
}
Float: {
    "float": 1.5
}
String: {
    "text": ""
}
Description: {
    "text": ""
}
RE Pattern Match: {
    "text": "" # This is an "RE Pattern Match" field
}
Multiselect: {
    "items": ["item 1", "item 2"]
}
Select: {
    "items": ["item 1"]
}
Location: { # This is a "Geo Location" field
    "projection": "",
    "coordinates": ""
}
# Either by_id or by_name can be used here.  If both are provided, by_name
# is ignored.
Taxonomy: {
    "taxonomy_values": [
        {
            "by_id": [5]
        },
        {
            "by_name": "node_1"
        }
    ]
}
URL: {
    "text": ""
}
User: {
    "user": "/api/artifacts/user-229"
}
User Company: {
    "object": "/api/artifacts/company-999"
}
BCDE Object: {
    "object": "/api/artifacts/document-1234"
}
```

## <a name="sample-output-from-the-script"></a> Sample output from the scripts

* See [set_metadata_debug.log](../python/data/set_metadata_debug.log)
for an example of the full HTTP headers that are sent.
* See [set_metadata_payload.txt](../python/data/set_metadata_payload.txt) for
an example of the JSON returned.
