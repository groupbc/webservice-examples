# BCDE Webservices Examples

This repository contains examples of how to use the BCDE WebServices API.

These example can be found on GitLab.com at https://gitlab.com/groupbc/webservice-examples.

## Standard Guides

 * [Authentication](guides/authenticate.md)
 * [JWT Authentication](guides/authenticate_jwt.md)
 * [Creating a Project Workspace](guides/create_project.md)
 * [Creating a Document](guides/create_document.md)
 * [Getting and Setting Metadata](guides/set_metadata.md)
 * [Performing Simple Searches](guides/simple_search.md)
 * [Triggers (event subscriptions)](guides/triggers.md)
 * [Streaming Files](guides/streaming_files.md)

## Programming Languages

 * [Python](python/README.md) - A good starting point for developers, both
    those with Python experience as well as those without.

## Other Resources

 * [The BCDE Web Services' blog posts](https://cobweb.businesscollaborator.com/pub/english.cgi/0/29764263)
 * Web Services demo
    * This is a demo that helps you get up and running with your registered
    app, walking through the three-legged authentication flow and allowing
    you to make requests.
    * On each server, this demo is available at
    `https://[server_address]/bc/bc.cgi?op=web_service_demo` where
    `[server_address]` is the name of the BCDE server.
 * Web Services API documentation
    * This is a list of all of the endpoints available on that server,
    including details on how to use the endpoints.
    * On each server, the documentation is available at
    `https://[server_address]/bc/bc.cgi?op=web_service_documentation` where
    `[server_address]` is the name of the BCDE server.
